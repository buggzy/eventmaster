<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\EventType;
use Yii;
use yii\helpers\Url;
use app\models\User;


class EventtypeController extends Controller {

    public function beforeAction($action){
		User::requireAdminAccess();	
		return parent::beforeAction($action);
    }

	public function actionIndex($parent = 0){

		$model = EventType::findByPk($parent);
		$grand = $model ? $model-> parent_id : false;

		$provider = new ActiveDataProvider([
		    'query' => EventType::find()->where(['parent_id'=>$parent]),
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		return $this->render('index',
		[
			'dataProvider' => $provider,
			'parent' => $parent,
			'grand' => $grand
		]);
	}

	public function actionRemove($id){
		$model = EventType::findByPk($id);
		$parent = $model->parent_id;
		$model->delete();
		return $this->redirect(['eventtype/index', 'parent'=>$parent]);
	}

	public function actionView($id, $parent = 0){
		
		if($id == 'new'){
			$model = new EventType();
			$model->parent_id = $parent;
		} else {
			$model = EventType::findByPk($id);
		}
    	if($model->load(Yii::$app->request->post())){
    		$model->save();
    		if(isset(Yii::$app->request->post()['save']))
    			$this->redirect(['eventtype/index', 'parent'=>$parent]);
    	}

		return $this->render('view', ['model'=>$model]);
	}

	
	
}
?>