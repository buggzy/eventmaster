<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Messages;
use app\models\ContactForm;
use app\models\User;
use app\models\Order;
use app\models\Moderation;
use app\models\Statistics;
use app\models\SystemSettings;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'thumb' => 'iutbay\yii2imagecache\ThumbAction',

        ];
    }

    public function actionIndex()
    {
    	if(Yii::$app->user->getIsGuest()){
    		$orders = Order::find()->count();
    		$servitors = User::find()->andWhere(['user_type_id'=>User::TYPE_SERVER])->count();
    		return $this->render('index', ['orders'=>$orders, 'servitors'=>$servitors]);
    	}
    	else
    		return $this->redirect(['site/cabinet']);
    }
    
    public function actionRef($id){
    	Yii::$app->session->set('ref', $id);
    	return $this->goHome();
    }
    
    public function actionPhone($phone){
    	$model = new Order();
    	$model->phone_add = $phone;
    	$model->obtainvericode();
    	return '[]';
    }
    
    public function actionCabinetClient(){
		$user = User::findIdentity(Yii::$app->user->id);
		$orders = Order::getOrders($user->id);
		$messages = Messages::GetList($user->id);
		$favs = $user->_fav_ids;

		if( (count($orders) > 0) && (!$user->profileCompleted()) )
			return $this->redirect(['user/view', 'id'=>$user->id]);
			
		return $this->render('cabinet_client', ['user'=>$user, 'orders'=>$orders, 'messages'=>$messages, 'favs'=>$favs]);
    }
    public function actionCabinetPartner(){
		$user = User::findIdentity(Yii::$app->user->id);
		return $this->render('cabinet_partner', ['user'=>$user]);
    }
    public function actionCabinetServer(){
		$user = User::findIdentity(Yii::$app->user->id);
		$orders = Order::getOrders($user->id);
		$messages = Messages::GetList($user->id);
		return $this->render('cabinet_server', ['user'=>$user, 'orders'=>$orders, 'messages'=>$messages]);
    }
    public function actionCabinetAdmin(){
		$moderation = new Moderation();
		$statistics = new Statistics();
		return $this->render('cabinet_admin', ['moderation'=>$moderation, 'statistics'=>$statistics]);
    }
    
    public function actionCabinet(){
    	
    	if(Yii::$app->user->getIsGuest())
			return $this->goHome();

    	if(User::isThisRole(User::TYPE_ADMIN))
    		return $this->actionCabinetAdmin();

    	if(User::isThisRole(User::TYPE_CLIENT))
    		return $this->actionCabinetClient();

    	if(User::isThisRole(User::TYPE_SERVER))
    		return $this->actionCabinetServer();
    		
    	if(User::isThisRole(User::TYPE_PARTNER))
    		return $this->actionCabinetPartner();
    		
    	$this->redirect(['user/choose']);

    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
