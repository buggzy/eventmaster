<?

namespace app\controllers;

use yii\web\Controller;
use app\models\Payments;
use app\models\PaymentsTypes;
use app\models\MailTemplate;
use app\models\User;
use app\models\SystemSettings;
use Yii;
use yii\helpers\Url;

class PaymentsController extends Controller {

	public function beforeAction($action)
	{            
	    if ($action->id == 'yandex') {
	        $this->enableCsrfValidation = false;
	    }

    	return parent::beforeAction($action);
	}
	
    public function actionYandex(){
    	
    	$label = Yii::$app->request->post('label');
    	$amount = max(Yii::$app->request->post('amount'), Yii::$app->request->post('withdraw_amount'));
    	
    	$payment = Payments::findByPk($label);

    	MailTemplate::sendMail(SystemSettings::getVal('admin_email'), 'ADMIN_PAYMENT_NOTIFY', ['SUM'=>$amount, 'ORDER'=>$payment->id, 'SYSTEM'=>'yandex']);

    	if($amount >= $payment->_payment_type->cost){
    		$payment->status = 1;
    		$payment->save();
    		$user = User::findIdentity($payment->owner_id);
    		$period = $payment->_payment_type->period * 60 * 60; // часов
    		$paid_until = $user->pay_until ? $user->pay_until : 0;
    		$paid_until = max($paid_until, time() + $period);
    		$user->pay_until = $paid_until;
    		$user->save();
    		if($user->email)
		    	MailTemplate::sendMail($user->email, 'USER_PAYMENT_NOTIFY', ['SUM'=>$amount, 'TITLE'=>$payment->_payment_type->title, 'SYSTEM'=>'yandex', 'DATE_EXPIRE' => date('d-m-Y', $paid_until)]);
		    	
    		return "OK";
    	}
    	
    }
    
    public function actionShared()
    {
        if (Yii::$app->user->getIsGuest())
            return JSON_encode(['guest'=>true]);

		$user = Yii::$app->user->identity;
		
		$last_shared = $user->last_shared;
		if($user->_paid_now || ($last_shared > time() - 60*60*24*30 ))
            return JSON_encode(['paid'=>$user->_paid_now, 'last_shared'=>$last_shared]);
			
		$user->last_shared = time();
		$period = PaymentsTypes::findByPk(1)->period;
		$user->pay_until = time() + $period * 60 * 60;
		$user->save();
		
		$payments = new Payments();
		$payments->payment_type_id = 1;
		$payments->owner_id = $user->id;
		$payments->status = 1;
		$payments->save();
		
//        return JSON_encode(['redirect'=>Url::toRoute(['payments/subscribe'])]);
        return JSON_encode([]);

	}

	
    public function actionSubscribe()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $user = Yii::$app->user->identity;
        
        if($user->_paid_now){
	        return $this->render('paid-already', [
	            'user' => $user,
	        ]);
        }
       
        // todo - форма с выбором типа подписки, платежной системы и вторая форма с автоотправкой в платежную систему
        $payment = new Payments();
        if ($payment->load(Yii::$app->request->post())) {
        	$payment->owner_id = Yii::$app->user->id;
        	$payment->status = 0;
        	$payment->save();
        	if($payment->_payment_type->cost){

		        return $this->render('payment-system', [
		            'payment' => $payment
		        ]);
        	}
        	if($payment->_payment_type->url){
        		return $this->redirect($payment->_payment_type->url);
        	}
        	return $this->goHome();
        }

        return $this->render('payment-type-select', [
            'payment' => $payment
        ]);
    }

	
	
	public function actionTransactions(){
		
		User::requireAdminAccess();
		
		$model = new Payments();
		return $this->render('transactions', ['model'=>$model]);
	}

	public function actionTypes(){
		
		User::requireAdminAccess();
		
		$model = new PaymentsTypes();
		return $this->render('types', ['model'=>$model]);
	}

	public function actionTransaction($id){

		User::requireAdminAccess();
		
		if($id == 'new'){
				$model = new Payments();
		} else {
			$model = Payments::findByPk($id);
		}
		$model->scenario = 'admin';
    	if($model->load(Yii::$app->request->post())){
    		$model->save();
    		if(isset(Yii::$app->request->post()['save']))
    			$this->redirect(['payments/transactions']);
    	}

		return $this->render('transaction', ['model'=>$model]);
	}
	
	public function actionType($id){

		User::requireAdminAccess();
		
		if($id == 'new'){
				$model = new PaymentsTypes();
		} else {
			$model = PaymentsTypes::findByPk($id);
		}
    	if($model->load(Yii::$app->request->post())){
    		$model->save();
    		if(isset(Yii::$app->request->post()['save']))
    			$this->redirect(['payments/types']);
    	}

		return $this->render('type', ['model'=>$model]);
	}
	
}
?>