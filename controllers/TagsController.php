<?
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Tags;
use app\models\User;


class TagsController extends Controller {

    public function beforeAction($action){
		User::requireAdminAccess();	
		return parent::beforeAction($action);
    }

	public function actionIndex(){
		
		$model = new Tags();
		return $this->render('index',
		[
			'model' => $model
		]);
	}

	public function actionView($id, $parent = 0){
		
		if($id == 'new'){
			$model = new Tags();
		} else {
			$model = Tags::findByPk($id);
		}
    	if($model->load(Yii::$app->request->post())){
    		$model->save();
    		if(isset(Yii::$app->request->post()['save']))
    			$this->redirect(['tags/index']);
    	}

		return $this->render('view', ['model'=>$model]);
	}


	
}
?>
