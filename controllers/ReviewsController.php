<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\ImportedReviews;
use Yii;
use yii\helpers\Url;
use app\models\User;


class ReviewsController extends Controller {

    public function beforeAction($action){
		User::requireAdminAccess();	
		return parent::beforeAction($action);
    }

	public function actionIndex(){

		Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());

		$provider = new ActiveDataProvider([
		    'query' => ImportedReviews::find(),
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		return $this->render('index',
		[
			'dataProvider' => $provider,
		]);
	}

	public function actionRemove($id){
		$model = ImportedReviews::findByPk($id);
		$model->delete();
		return $this->goBack();
	}

	public function actionView($id, $parent = 0){
		
		if($id == 'new'){
			$model = new ImportedReviews();
		} else {
			$model = ImportedReviews::findByPk($id);
		}
    	if($model->load(Yii::$app->request->post())){
    		if(isset(Yii::$app->request->post()['save']))
	    		$model->save();
			$this->goBack();
    	}

		return $this->render('view', ['model'=>$model]);
	}

	
	
}
?>