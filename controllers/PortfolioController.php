<?
namespace app\controllers;

use yii\web\Controller;
use app\models\Portfolio;
use Yii;
use app\models\User;
use yii\web\UploadedFile;
use app\models\MailTemplate;


class PortfolioController extends Controller {

	public function actionIndex($id){
		
		$model = new Portfolio();
		$model->filter_id = $id;
		$moderation = ($model->filter_id == 'unapproved');
		
		$writeable = (!Yii::$app->user->getIsGuest()) && (User::isThisRole(User::TYPE_ADMIN) || (Yii::$app->user->id == $id));
		
		return $this->renderAjax('_index', ['model'=>$model, 'id'=>$id, 'writeable'=>$writeable, 'moderation'=>$moderation]);
	}

	public function actionModeration(){
		return $this->render('index');
	}

	public function actionApprove($id){
		User::requireAdminAccess();
		$model = Portfolio::findByPK($id);
		if(!$model)
			return false;
			
		$model->moderated = 1;
		$model->save();
		return true;
	}

	public function actionAddlink($id){
		User::requireBeUserId($id);

		$link = Yii::$app->request->post('addlink');
		
		if(preg_match('/youtube/', $link)){
			if(preg_match('/v=(.*)$|&/', $link, $m)){
	        	$model = new Portfolio();
	        	$model->owner_id = $id;
	        	$model->path = $m[1];
	        	$model->type = Portfolio::TYPE_YOUTUBE;
	        	$model->save();
				return true;
			}
		}
		
		
		if(preg_match('/<iframe.*src="(.*?)".*\/iframe>/', $link, $m)){
        	$model = new Portfolio();
        	$model->owner_id = $id;
        	$model->path = $m[1];
        	$model->type = Portfolio::TYPE_IFRAME;
        	$model->save();
			return true;
		}
		
		return false;
		
	}

	public function actionUpload($id){
		
		User::requireBeUserId($id);
		
		$files = UploadedFile::getInstancesByName('ajax_portfolio_upload');
		foreach($files as $file){
			if(!in_array(strtolower($file->getExtension()), ['jpg', 'jpeg', 'png', 'gif']))
				continue;
				
	    	$name = md5($file->getBaseName() . $file->size) . '.' . $file->getExtension();
	    	$fname = Yii::getAlias('@webroot/uploads/' . $name);
	        if($file->saveAs($fname)){
	        	$model = new Portfolio();
	        	$model->owner_id = $id;
	        	$model->path = $name;
	        	$model->type = Portfolio::TYPE_IMAGE;
	        	$model->save();
	        }
		}
		
		return true;
	}

	public function actionRemove($id){
		
		$model = Portfolio::findByPk($id);
		
		
		if(($model->owner_id != Yii::$app->user->id) && ($model->_owner->email)){
			MailTemplate::sendMail($model->_owner->email, 'PORTFOLIO_ITEM_REJECTED');
		}
		User::requireBeUserId($model->owner_id);
		
		$model->delete();
		
		return true;

	}
		
		
}

?>