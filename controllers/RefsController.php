<?
namespace app\controllers;

use app\models\RefsPayments;
use app\models\User;
use yii\web\Controller;


class RefsController extends Controller {
	
	public function actionIndex(){
		
		User::requireAdminAccess();
		
		return $this->render('index', ['dataProvider'=>User::hasReferals()]);
	}
	
	public function actionView($id){

		return $this->render('view', ['dataProvider' => RefsPayments::myReferals($id)]);
	}
	
}

