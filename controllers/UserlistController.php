<?
namespace app\controllers;

use yii\web\Controller;
use app\models\User;
use app\models\Userlist;
use Yii;


class UserlistController extends Controller {

	public function actionIndex(){
		
		$model = new Userlist();
		
		$model->load(Yii::$app->request->post());
			
		if(User::isThisRole(USER::TYPE_ADMIN)){
			return $this->render('index_admin', [ 'model'=>$model ]);
		} elseif(Yii::$app->user->getIsGuest() || User::isThisRole(USER::TYPE_CLIENT)) {
			return $this->render('index', [ 'model'=>$model, 'filter'=>true ]);
		} else {
			return $this->goHome();
		}

	}
	public function actionFavs(){
		
		if(Yii::$app->user->getIsGuest())
			return $this->goHome();
		
		$model = new Userlist();
		
		$model->favs = 1;
		
		$model->load(Yii::$app->request->post());
			
		return $this->render('index', [ 'model'=>$model, 'filter'=>false,
			'title' => 'Избранные исполнители', 'notfound'=>'Список избранных исполнителей пуст' ]);

	}
}
?>
