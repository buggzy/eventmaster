<?
namespace app\controllers;

use yii\web\Controller;
use app\models\Order;
use Yii;
use yii\helpers\Url;
use app\models\User;
use app\models\Messages;


class PingerController extends Controller {
	
	public function actionIndex(){
		
		$retval = [];
		
		if(Yii::$app->user->getIsGuest()){
			$retval['goAway'] = true;
		} else {
			$user = Yii::$app->user->identity;
			$user->last_online = time();
			$user->save();
			$retval['user'] = $user->fio;
			
			$messages = new Messages();
			$retval['messages_unread'] = $messages->GetMessagesUnread()->count();
		}
		
		return JSON_encode($retval);
	}
}
