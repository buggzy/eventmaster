<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Regions;
use Yii;
use yii\helpers\Url;
use app\models\User;


class RegionsController extends Controller {

    public function beforeAction($action){
		User::requireAdminAccess();	
		return parent::beforeAction($action);
    }

	public function actionIndex(){

		$provider = new ActiveDataProvider([
		    'query' => Regions::find()->orderBy('title'),
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		Yii::$app->getUser()->setReturnUrl(Yii::$app->request->url);
		
		return $this->render('index',
		[
			'dataProvider' => $provider
		]);
	}

	public function actionModeration(){

		$provider = new ActiveDataProvider([
		    'query' => Regions::find()->where(['moderated'=>0])->orderBy('title'),
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		Yii::$app->getUser()->setReturnUrl(Yii::$app->request->url);
		
		return $this->render('index',
		[
			'dataProvider' => $provider
		]);
	}

	public function actionApprove($id){
		$model = Regions::findByPk($id);
		if($model){
			$model->moderated = 1;
			$model->save();
		}
		$this->goBack();
	}

	public function actionRemove($id){
		$model = Regions::findByPk($id);
		if($model){
			$model->delete();
		}
		$this->goBack();
	}


	public function actionView($id){
		
		if($id == 'new'){
			$model = new Regions();
		} else {
			$model = Regions::findByPk($id);
		}
    	if($model->load(Yii::$app->request->post())){
    		$model->save();
    		if(isset(Yii::$app->request->post()['save']))
    			$this->goBack();
    	}

		return $this->render('view', ['model'=>$model]);
	}

	
	
}
?>