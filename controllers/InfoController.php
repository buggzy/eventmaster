<?

namespace app\controllers;

use yii\web\Controller;
use app\models\InfoPages;
use app\models\User;
use Yii;

class InfoController extends Controller {
	
	public function actionIndex(){
		
		User::requireAdminAccess();
		
		$model = new InfoPages();
		return $this->render('index', ['model'=>$model]);
	}

	public function actionView($id){

		User::requireAdminAccess();
		
		if($id == 'new'){
				$model = new InfoPages();
		} else {
			$model = InfoPages::findByPk($id);
		}
    	if($model->load(Yii::$app->request->post())){
    		$model->save();
    		if(isset(Yii::$app->request->post()['save']))
    			$this->redirect(['info/index']);
    	}

		return $this->render('view', ['model'=>$model]);
	}
	
	public function actionRender(){
		$page = InfoPages::findByUrl(Yii::$app->request->getUrl());
		if(!$page)
			throw new \yii\web\NotFoundHttpException("Неизвестная страница");
			
		return $this->render('page', ['page'=>$page]);

	}
}
?>