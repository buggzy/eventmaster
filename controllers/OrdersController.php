<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Order;
use app\models\MailTemplate;
use Yii;
use yii\helpers\Url;
use app\models\User;
use app\models\Orderlist;
use app\models\ImportRequest;
use app\models\SystemSettings;


class OrdersController extends Controller {

	public function actionIndex($id = null){

		$userid = Yii::$app->user->getIsGuest() ? -1 : Yii::$app->user->id;
		
		$time = time();
		if(User::isThisRole(User::TYPE_ADMIN))
			$query = Order::find();
		else
			$query = User::isThisRole(User::TYPE_CLIENT) ?
				Order::find()->where(['owner_id'=> $userid]) :
				Order::find()
					-> andWhere("(auto_hide is null) or (auto_hide >= $time)")
					-> andWhere("unix_timestamp(str_to_date(order_date, '%d-%m-%Y')) >= $time");
				
		if(($userid == -1) || User::isThisRole(User::TYPE_SERVER))
			$query = $query -> andWhere("order_moderated = 1");

		$searchModel = new Orderlist;
		$searchModel->load($_REQUEST);
		if($id)
			$searchModel->id = $id;

		$searchModel->alterQuery($query);
		
		$query = $query->orderBy(['id'=>SORT_DESC]);

		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'sort' => false,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
		
		
		if(User::isThisRole(User::TYPE_ADMIN))
			return $this->render('index-admin',
			[
				'dataProvider' => $provider,
				'searchModel' => $searchModel
			]);
		
		if(User::isThisRole(User::TYPE_CLIENT))
			return $this->render('index-client',
			[
				'dataProvider' => $provider,
				'searchModel' => $searchModel
			]);

		// гость или исполнитель
		return $this->render('index-server',
			[
				'dataProvider' => $provider,
				'searchModel' => $searchModel
			]);
			
	}
	
	public function actionFavs(){

		if(!User::isThisRole(User::TYPE_SERVER))
			return Yii::$app->response->redirect(['user/login', 'backurl'=>Yii::$app->request->getUrl()]);

		$time = time();
		$query = Order::find()->where(['id'=>Yii::$app->user->identity->_fav_ids]);
		$query = $query
					-> andWhere("(auto_hide is null) or (auto_hide >= $time)")
					-> andWhere("unix_timestamp(str_to_date(order_date, '%d-%m-%Y')) >= $time");

		$query = $query->orderBy(['id'=>SORT_DESC]);

		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'sort' => false,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $this->render('index-server',
		[
			'dataProvider' => $provider,
			'searchModel' => false,
			'title' => 'Избранные заказы',
			'notfound' => 'Пока в избранные заказы ничего не добавлено'
		]);
		
	}

	public function actionFeedback_moderation(){
		User::requireAdminAccess();
		$model = new Order();
		return $this->render('feedback_moderation', ['model'=>$model]);
	}
	
	public function actionFeedback_approve($id){
		User::requireAdminAccess();
		$model = Order::findByPk($id);
		$model->feedback_moderated = 1;
		$model->save();
		$this->redirect(['orders/feedback_moderation']);
	}
	
	public function actionFeedback_cancel($id){
		User::requireAdminAccess();
		$model = Order::findByPk($id);
		$model->servitor_id = 0;
		$model->save();
		$this->redirect(['orders/feedback_moderation']);
	}
	
	public function actionFeedback($order, $user){
		
		$order = Order::findByPK($order);
		$order->scenario = 'feedback';
		$user = User::findIdentity($user);
		if(!$order || !$user)
			throw new \yii\web\NotFoundHttpException();

		User::requireBeUserId($order->owner_id);
    	if($order->load(Yii::$app->request->post())){
    		$order->servitor_id = $user->id;
    		$order->save();
    		$to = $user->email;
    		if($to){
    			MailTemplate::sendMail($to, 'YOU_GOT_FEEDBACK', ['TITLE'=>$order->title, 'FIO'=>Yii::$app->user->identity->fio,'TEXT'=>$order->feedback_text]);
    		}
			return $this->redirect(['/dorepost']);
    	}
			
		return $this->render('feedback', ['order'=>$order, 'user'=>$user]);
		
	}
	
	public function actionAnon(){
		
		if(!Yii::$app->user->getIsGuest())
			return $this->redirect(['orders/view', 'id'=>'new']);
		
		$model = new Order();
		$model->owner_id = -1;
    	if($model->load(Yii::$app->request->post()) && $model->validate()){
    		$model->save();
			Yii::$app->session->set('attach_order', $model->id);
			Yii::$app->session->setFlash('header-messages', SystemSettings::getVal('anon_order'), false);
			return $this->redirect(['user/register']);
    	}
    	
		return $this->render('view', ['model'=>$model]);
		
	}

	public function actionImport(){
		
		if(!User::isThisRole(User::TYPE_SERVER))
			return $this->goHome();
		
		$model = new ImportRequest();
    	if($model->load(Yii::$app->request->post())){
    		$to = SystemSettings::getVal('admin_email');
    		MailTemplate::sendMail($to, 'IMPORT_REVIEWS',
    			['URL'=>Yii::$app->request->hostInfo . Url::toRoute(['user/reviews', 'id'=>Yii::$app->user->id]),'TEXT'=>$model->text]);

			Yii::$app->session->setFlash('header-messages', SystemSettings::getVal('request_accepted'), false);

			return $this->redirect(['site/cabinet']);
    	}
    	
		return $this->render('import', ['model'=>$model]);
		
	}

	public function actionView($id){

		if($id == 'new'){
			$model = new Order();
			$model->owner_id = Yii::$app->user->id;
			if(!Yii::$app->user->getIsGuest()){
				$user = Yii::$app->user->identity;
				$model->fio = $user->fio;
				$model->email = $user->email;
				if(in_array($user->phone, $model->verifiedPhones()))
					$model->phone = $user->phone;
				else
					$model->phone_add = $user->phone;
			}
		} else {
			$model = Order::findByPk($id);
		}

		if(!$model)
			throw new \yii\web\NotFoundHttpException();

		User::requireBeUserId($model->owner_id, true);
		
    	if($model->load(Yii::$app->request->post()) && $model->validate()){
    		if(!User::isThisRole(User::TYPE_ADMIN)){
				$model->edited = 1 + $model->edited;
				$model->setAutoHide();
			}
				
			if(!User::isThisRole(User::TYPE_ADMIN))
				$model->order_moderated = 0;
				
    		$model->save();
    		
    		if(isset(Yii::$app->request->post()['save'])){
    			$owner_user = User::findIdentity($model->owner_id);
    			if($owner_user && (($owner_user->fio == '')||($owner_user->email == '')||($owner_user->email == '')) ){
	    			$this->redirect(Url::toRoute(['user/view', 'id'=>$model->owner_id]));
					Yii::$app->session->setFlash('header-messages', SystemSettings::getVal('fill_profile'), false);
    			}
	    		else
	    			$this->goBack();
    		}
    	}

		return $this->render('view', ['model'=>$model]);
	}

	public function actionRemove($id){
			$model = Order::findByPk($id);
			User::requireBeUserId($model->owner_id, true);
			$model->hidden = 1;
			$model->save();
			if(User::isThisRole(User::TYPE_ADMIN))
				return $this->redirect(['orders/index']);
			else
				return $this->goHome();
	}
	
	
}
?>