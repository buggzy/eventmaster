<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\MailTemplate;
use Yii;
use yii\helpers\Url;
use app\models\User;


class MailtemplatesController extends Controller {

    public function beforeAction($action){
		User::requireAdminAccess();	
		return parent::beforeAction($action);
    }

	public function actionIndex(){

		$provider = new ActiveDataProvider([
		    'query' => MailTemplate::find(),
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		return $this->render('index',
		[
			'dataProvider' => $provider
		]);
	}

	public function actionView($id){
		
		if($id == 'new'){
			$model = new MailTemplate();
		} else {
			$model = MailTemplate::findByPk($id);
		}
    	if($model->load(Yii::$app->request->post())){
    		$model->save();
    		if(isset(Yii::$app->request->post()['save']))
    			$this->redirect(['mailtemplates/index']);
    	}

		return $this->render('view', ['model'=>$model]);
	}

	
	
}
?>