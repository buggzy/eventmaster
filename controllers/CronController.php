<?
namespace app\controllers;

use yii\web\Controller;
use app\models\Order;
use app\models\Messages;
use app\models\MailTemplate;
use app\models\Payments;
use app\models\RefsPayments;
use app\models\SystemSettings;
use Yii;
use yii\helpers\Url;
use app\models\User;


class CronController extends Controller {
	public function actionIndex(){
		
		// вычисление реферальных платежей
		$payments = Payments::find()->where(['ref_given'=>0, 'status'=>1])->all();
		
		$perc = SystemSettings::getVal('referal_perc');
		
		foreach($payments as $payment){
			$type = $payment->_payment_type;
			$cost = $type->cost;
			$payment_period = $type->period * 60 * 60;

			$payment_begin = strtotime($payment->timestamp);
			$payment_end = $payment_begin + $payment_period;
			if($cost <= 0){ // платежи, по которым никогда не нужно начислений, отмечаем как обработанные
				$payment->ref_given = 1;
				$payment->save();
				continue;
			}
			
			if($payment_end > time()) // платежи, по которым начисления нужно вычислять потом, пока откладываем, никак не помечаем
				continue;

			// остальное (положительный платеж, период оплаты закончился) обрабатываем			
			echo "Обработан платеж ".$payment->id.' начало '.$payment_begin.' конец '.$payment_end;
			
			// получаем контрагентов за оплаченный период - кому отправляли?
			$contragents_messages = Messages::find()->where(['owner_id'=>$payment->owner_id])->all();
			$ref_contragents = [];
			foreach($contragents_messages as $message){
				$recipient = $message->_recipient;
				if($recipient->ref_parent){
					$ref_contragents[$recipient->id] = $recipient;
				}
			}
			
			//var_dump($ref_contragents);
			if(!empty($ref_contragents)){
				$count = count($ref_contragents);
				foreach($ref_contragents as $ref_contragent){
					// начисляем рефералу приз
					$ref_payment = new RefsPayments;
					$ref_payment->owner_id = $ref_contragent->ref_parent;
					$ref_payment->for_id = $ref_contragent->id;
					$ref_payment->amount = round($cost / $count * $perc / 100, 2);
					$ref_payment->timestamp = time();
					echo "Начисляем ";var_dump($ref_payment);
					$ref_payment->save();
				}
			}
			
			$payment->ref_given = 1;
			$payment->save();
		}
		
		// письма с просьбой оценить окончившиеся мероприятия

		// выбрать давно закончившиеся мероприятия
		$time = time() - 7*60*60*24;
		$orders = Order::find()->where("unix_timestamp(str_to_date(order_date, '%d-%m-%Y')) < $time")->andWhere('final_letter = 1')->all();
		foreach($orders as $order){
			$order->scenario = 'final_letter';
			$to = $order->owner->email;
			if($to){
				echo "Последнее письмо с просьбой оценить заказ id=".$order->id." отправлено на $to\r\n";
				MailTemplate::sendMail($to, 'LAST_FINAL_MAIL',
				[
					'ORDER_TITLE' => $order->title,
					'ORDER_DATE' => $order->order_date,
					'FEEDBACK_URL' => Yii::$app->request->hostInfo . Url::toRoute(['message/deals', 'order'=>$order->id, 'user'=>$order->owner_id]),
					]);
			}
			$order->final_letter = 2;
			$order->save();
		}

		// выбрать как минимум вчера закончившиеся мероприятия		
		$time = time() - 60*60*24;
		$orders = Order::find()->where("unix_timestamp(str_to_date(order_date, '%d-%m-%Y')) < $time")->andWhere('final_letter = 0')->all();
		foreach($orders as $order){
			$order->scenario = 'final_letter';
			if(!$order->owner)
				continue;
				
			$to = $order->owner->email;
			if($to){
				echo "Первичное письмо с просьбой оценить заказ id=".$order->id." отправлено на $to\r\n";
				MailTemplate::sendMail($to, 'FIRST_FINAL_MAIL',
				[
					'ORDER_TITLE' => $order->title,
					'ORDER_DATE' => $order->order_date,
					'FEEDBACK_URL' => Yii::$app->request->hostInfo . Url::toRoute(['message/deals', 'order'=>$order->id, 'user'=>$order->owner_id]),
					]);
			}
			$order->final_letter = 1;
			$order->save();
		}
		
		// оповестить тех, чей заказ не отображается по причине неактивности
		$time = time();
		$orders = Order::find()->where(['autohide_notified'=>0])->andWhere('auto_hide is not null')->andWhere("auto_hide < $time")->all();
		foreach($orders as $order){
			
			$to = $order->owner->email;
			if($to){
				echo "Письмо об автоматическом скрытии заказа id=".$order->id." отправлено на $to\r\n";
				MailTemplate::sendMail($to, 'ORDER_AUTOHIDE',
				[
					'ORDER_TITLE' => $order->title,
					'ORDER_DATE' => $order->order_date,
					'URL' => Yii::$app->request->hostInfo . Url::toRoute(['message/index']),
				]);
			}
			$order->autohide_notified = 1;
			$order->save();
		}
	}
	
	public function actionDaily(){
		
		$notifyThem = [];
		
		$orders = Order::findAll(['subscribers_emailed'=>0]);
		foreach($orders as $order){
			$users = User::findAll(['event_type_id'=>$order->event_type_id, 'user_type_id'=>User::TYPE_SERVER]);
			foreach($users as $user){
				if(!isset($notifyThem[$user->id]))
					$notifyThem[$user->id] = [];
					
				$notifyThem[$user->id][] = $order->id;
			}
		}
		
		//var_dump($notifyThem);
		
		foreach($notifyThem as $user=>$orders){
			$user = User::findIdentity($user);
			if(!$user->email)continue;
			$args = ['FIO'=>$user->fio];
			$orders_list = '<ul>';
			foreach($orders as $order){
				$order = Order::findByPk($order);
				$order->scenario = 'cron_daily';
				$order->subscribers_emailed = 1;
				$order->save();
				$orders_list .= '<li>'.$order->title.'</li>';
			}
			$orders_list .= '</ul>';
			$args['ORDERS_LIST'] = $orders_list;
			$args['URL'] = Yii::$app->request->hostInfo . Url::toRoute(['orders/view', 'id'=>$order->id]);
			
			MailTemplate::sendMail($user->email, 'SERVITOR_DAILY', $args);
			echo "Отправлено письмо на ".$user->email." о ".count($orders)." новых заказах\r\n";
			
		}
	}

}
?>