<?

namespace app\controllers;

use yii\web\Controller;
use app\models\SystemSettings;
use app\models\User;
use Yii;

class SettingsController extends Controller {
	
	public function actionIndex(){
		
		User::requireAdminAccess();
		
		$model = new SystemSettings();
		return $this->render('index', ['model'=>$model]);
	}

	public function actionView($id){

		User::requireAdminAccess();
		
		if($id == 'new'){
				$model = new SystemSettings();
		} else {
			$model = SystemSettings::findByPk($id);
		}
    	if($model->load(Yii::$app->request->post())){
    		$model->save();
    		if(isset(Yii::$app->request->post()['save']))
    			$this->redirect(['settings/index']);
    	}

		return $this->render('view', ['model'=>$model]);
	}
	
}
?>