<?
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Messages;
use app\models\Order;
use app\models\MailTemplate;
use app\models\User;
use app\models\SystemSettings;
use yii\helpers\Url;


class MessageController extends Controller {
	
	public function actionIndex(){
		if(Yii::$app->user->getIsGuest())
			return $this->redirect(['user/login', 'backurl'=>Yii::$app->request->getUrl()]);
			
		$provider = Messages::GetList(Yii::$app->user->id);
		return $this->render('index', ['provider'=>$provider]);
	}
	
	public function actionDeal($order){
		$order = Order::findByPk($order);
		if(!$order)
			throw new NotFoundException();

		$user_id = $order->owner_id;
			
		return $this->redirect(['message/user', 'id'=>$user_id, 'order'=>$order->id]);
	}
	
	public function actionDeals($user = null, $order = null){
		
		if($user)
			User::requireBeUserId($user);
		
		if(Yii::$app->user->getIsGuest())
			return $this->goHome();
		$provider = Messages::GetTreeList(Yii::$app->user->id, $order);
		return $this->render('deals', ['provider'=>$provider]);
	}

	public function actionUser($id, $order = 0, $me = 0){
		
		if($me != 0)
			User::requireBeUserId($me);
		
		$recipient = User::findIdentity($id);
		if(!$recipient)
			throw new NotFoundException();
		
		if(Yii::$app->user->getIsGuest())
			return Yii::$app->response->redirect(['user/login', 'backurl'=>Yii::$app->request->getUrl()]);

		$user = Yii::$app->user->identity;
		
		if(!$user->fio || !$user->email || !$user->phone){
			Yii::$app->session->setFlash('header-messages', SystemSettings::getVal('fill_profile_servitor'), false);
			Yii::$app->getUser()->setReturnUrl(Yii::$app->request->url);
			return $this->redirect(['user/view', 'id'=>$user->id]);
		}

		$model = new Messages;
		$model->recipient = $id;
		$model->owner_id = $user->id;
		$model->order_id = $order;
		
		$order_mod = false;
		if($order){
			$order_mod = Order::findByPk($order);
			if($order_mod){
				// если это заказчик, то обнулить данные по отсчету автоматического скрытия
				if(User::isThisRole(User::TYPE_CLIENT)){
					$order_mod->resetAutoHide();
					$order_mod->save();
				}
			}
		}

		if(!$user->_paid_now && ($user->user_type_id == User::TYPE_SERVER) && ('0' == count($model->GetOutgoing())) )
			$this->redirect(['payments/subscribe', 'backurl'=>Yii::$app->request->getUrl()]);

		if($model->load(Yii::$app->request->post())){
			// если это исполнитель, то запустить отсчет автоматического скрытия по неактивности заказчика (если он не был запущен ранее)
			if($order_mod && User::isThisRole(User::TYPE_SERVER) && !$order_mod->auto_hide){
				$order_mod->setAutoHide();
				$order_mod->save();
			}
			$model->timestamp = time();
			$model->save();
			$to = $model->_recipient->email;
			if($to){
				if($model->_recipient->_paid_now){
					MailTemplate::sendMail($to, 'MESSAGE_NOTIFY', [
						'USER' => $model->_owner->fio, 'MESSAGE'=>$model->text, 'URL' => Yii::$app->request->hostInfo . Url::toRoute(['message/user', 'me'=>$id, 'order'=>$order, 'id'=>$user->id])
					]);
				} else {
					MailTemplate::sendMail($to, 'MESSAGE_NOTIFY_NOTEXT', [
						'USER' => $model->_owner->fio, 'URL' => Yii::$app->request->hostInfo . Url::toRoute(['message/user', 'me'=>$id, 'order'=>$order, 'id'=>$user->id])
					]);
				}
			}
			return $this->redirect(['message/user', 'id'=>$id, 'order'=>$order]);
		}
		
		$model->MarkRead();
		
		return $this->render('user', ['recipient'=>$recipient, 'model'=>$model]);
	}
}
