<?
namespace app\controllers;

use rmrevin\yii\ulogin\AuthAction;
use yii\web\Controller;
use Yii;
use app\models\LoginForm;
use app\models\ResetPassword;
use app\models\User;
use app\models\MailTemplate;
use app\models\Order;
use app\models\Payments;
use app\models\ajaxFileUploads;
use yii\web\UploadedFile;
use yii\helpers\Url;
use Html2Text\Html2Text;


class UserController extends Controller
{
	
	private $doUlogin = false;
	
	public function beforeAction($action)
	{            
	    if (in_array($action->id, ['ulogin'])) {
	        $this->enableCsrfValidation = false;
	    }

    	return parent::beforeAction($action);
	}

    public function actions()
    {
        return [
            // ...
            'ulogin' => [
                'class' => AuthAction::className(),
                'successCallback' => [$this, 'uloginSuccessCallback'],
                'errorCallback' => function($data){
                    \Yii::error($data['error']);
                },
            ]
        ];
    }
    

    public function actionSetpass($token){
    	$model = User::findOne(['reset'=>$token]);
    	if(!$model)
	    	return $this->render('reset', ['model'=>$model, 'status'=>3]);
			
		$model->scenario = 'setpass';
		if($model->load(Yii::$app->request->post() )){
			if($model->validate()){
				$model->password = $model->password1;
				$model->reset = null;
				$model->save();
				return $this->redirect(['user/login']);
				
			}
		}
			
		return $this->render('setpass', ['model'=>$model]);

    }
    
    public function actionMt($to){

            $message = Yii::$app->mailer->compose();
            
            return $message -> setFrom('info@eventblaster.ru')
		    	->setTo($to)
		    	->setSubject('test')
    	    	->setHtmlBody('test')
    	    	->setTextBody((new Html2Text('test'))->getText())
		    	->send();

    }

    
    public function actionReset()
    {
        $model = new ResetPassword();
        
		if ($model->load(Yii::$app->request->post())
		        && Yii::$app->recaptcha->verifyResponse($_SERVER['REMOTE_ADDR'], Yii::$app->request->post('g-recaptcha-response')))
		{
			$user = User::findAll(['username'=>$model->username]);
			if(!$user) // не найдена почта
		    	return $this->render('reset', ['model'=>$model, 'status'=>2]);
		    	
		    foreach($user as $_user){
			    $reset = md5($_user->id . time());
			    
			    $_user->reset = $reset;
			    MailTemplate::sendMail($_user->username, 'PASSWORD_RESET', [
			    		'URL' => Yii::$app->request->hostInfo . Url::toRoute(['user/setpass', 'token'=>$reset]),
			    		'FIO' => $_user->fio
			    	]);
			    }
			    
			    $_user->save();
		    	
	    	return $this->render('reset', ['model'=>$model, 'status'=>1]);
		}
        
    	return $this->render('reset', ['model'=>$model, 'status'=>0]);
    }
    
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('switch');
        }
        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->successRedirect();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionFav_toggle($id){

    	if(Yii::$app->user->getIsGuest())
    		return $this->goHome();
    		
    	$model = Yii::$app->user->identity;
		if(!$model)
			throw new \yii\web\NotFoundHttpException();
			
		$ids = $model->fav_ids ? preg_split('/;/', $model->fav_ids) : [];
		if(!in_array($id, $ids)){ // добавить
			$ids[] = $id;
			
			$model->fav_ids =  join(';', $ids);
			$model->save();
			return JSON_encode(['addClass'=>'in-favs']);
		} else { // убавить
			$ids = array_diff($ids, [$id]);
			$model->fav_ids =  join(';', $ids);
			$model->save();
			return JSON_encode(['removeClass'=>'in-favs']);
			
		}
    	
    }
    
    public function actionChoose(){
    	if(Yii::$app->user->getIsGuest())
    		return $this->goHome();
    		
    	$id = Yii::$app->user->id;
    	$model = User::findIdentity($id);
		if(!$model)
			throw new \yii\web\NotFoundHttpException();

		if($model->user_type_id)
			$this->goHome();
    	if($model->load(Yii::$app->request->post()) && $model->validate()){
    		$model->save();
	    	if($model->user_type_id){
	    		$model->registerGreeting();
	    	}

    		$this->goHome();
    	}
			
		return $this->render('choose', ['model'=>$model]);
    }


    public function actionRegister($type=NULL){
    	$model = new LoginForm();
    	
    	$model->type = $type;

		$order_id = Yii::$app->session->get('attach_order');
		$ref_id = Yii::$app->session->get('ref');


		if($order_id){
			$order = Order::findByPk($order_id);
	    	if($order){
		    	$model->username = $order->email;
	    	}
		}
		
		if($ref_id)
			$model->ref_parent = $ref_id;

        if ($model->load(Yii::$app->request->post()) && $model->register()) {
			return $this->successRedirect();
        }

        return $this->render('login', [
        	'model' => $model,
        	'message' => 'Регистрация',
        	]);
    }

	public function actionReviews($id, $type = 'total'){
		$user = User::findIdentity($id);
		if(!$user)
			throw new \yii\web\NotFoundHttpException();
			
		return $this->render('reviews', ['reviews'=>$user->_reviews, 'user'=>$user, 'type'=>$type]);
	}
    
    public function actionView($id)
    {
    	$model = User::findIdentity($id);
    	
		if(!$model)
			throw new \yii\web\NotFoundHttpException();
    	
    	$me = Yii::$app->user->getIsGuest() ? false : Yii::$app->user->id;
    	$meAdmin = User::isThisRole(User::TYPE_ADMIN);
    	
    	$hasWriteAccess = $meAdmin || ($me == $id);
    	
    	if(User::isThisRole(User::TYPE_SERVER))
    		$model->scenario = 'server_profile';
    	
    	if($hasWriteAccess){
    		// попробовать автозаполнение из заказов
    		if(!$model->fio || !$model->email || !$model->phone){
    			$order = Order::findOne(['owner_id'=>$id]);
    			if($order){
	    			if(!$model->fio)
	    				$model->fio = $order->fio;
	    			if(!$model->email)
	    				$model->email = $order->email;
	    			if(!$model->phone)
	    				$model->phone = $order->phone;
    			}
    		}

	    	if($model->load(Yii::$app->request->post()) && $model->validate()){
	    		
	    		
	    		if($model->password1)
	    			$model->password = $model->password1;
	    		
	            $file = UploadedFile::getInstanceByName('User[avatar]');
	            if($file){
		        	$name = md5($file->getBaseName() . $file->size) . '.' . $file->getExtension();
		        	$fname = Yii::getAlias('@webroot/uploads/' . $name);
		            if($file->saveAs($fname)){
		            	$model->avatar = $name;
		            }
	            }
	            
	            if(!$meAdmin) {
	            	$moderatedChanged = ($model->comments != $model->getOldAttribute('comments'));
	            	if($moderatedChanged)
	            		$model->moderated = false;
	            }
	            
	            if( ($model->getOldAttribute('email') != $model->email) && ($model->getOldAttribute('email') == $model->username) ){
	            	// исправить username при смене почты, если почта совпадает с юзернеймом
	            	$model->username = $model->email;
	            }
	            
	    		$model->save();
	    		if(isset(Yii::$app->request->post()['save'])){
	    			if(User::isThisRole(User::TYPE_ADMIN))
		    			$this->redirect(Url::toRoute('userlist/index'));
		    		else
		    			$this->goBack();
	    		}
	    	}
	    	return $this->render('view-writeable', ['model'=>$model]);
    	} else {
    		if(Yii::$app->request->isPost)
				return $this->goBack();
			else
				return $this->render('view-readonly', ['model'=>$model]);
    	}
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	protected function successRedirect(){
			$backurl = Yii::$app->request->getQueryParam('backurl');
			if($backurl){
				return Yii::$app->response->redirect($backurl);
			}
			else
				return $this->goHome();
		
	}

    public function uloginSuccessCallback($attributes)
    {
    	
		$model = new LoginForm();
		
		Yii::$app->user->login($model->ulogin($attributes));
		return $this->successRedirect();
    }
}
?>