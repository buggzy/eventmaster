<?php
namespace app\models;
use Yii;
use yii\base\Model;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $ref_parent;
    
    private $_user;
    public $type = NULL;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            ['username', 'email'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    
    public function attributeLabels(){
    	return [
    		'username' => 'Электронная почта',
    		'password' => 'Пароль'
    		];
    }
    
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
    		}
        }
        return false;
    }
    
    public function ulogin($attributes){
		$fio = [];
		if(isset($attributes['first_name']))$fio[] = $attributes['first_name'];
		if(isset($attributes['last_name']))$fio[] = $attributes['last_name'];
		$fio = join($fio, ' ');
		$email = isset($attributes['email']) ? $attributes['email'] : '';
		$ident = $attributes['identity'];

		$key = md5($ident) . '@ulogin';
		$user = User::findByUsername($key);
		if(!$user){
			$user = new User();
			$user->scenario = 'register';
			$user->username = $key;
			$user->email = $email;
			$user->password = md5($key . time());
			$user->fio = $fio;
			$user->phone = (isset($attributes['phone'])) ? $attributes['phone'] : '';
			$user->region = (isset($attributes['city'])) ? $attributes['city'] : '';
			
			$user->ref_parent = $this->ref_parent;
			
			if(isset($attributes['big_picture']))
				$user->uploadAvatarFile($attributes['big_picture']);
				
			
			if(!$user->save()){
				Yii::$app->session->setFlash('header-messages', 'Невозможно зарегистрироваться: email <b>'.$user->email.'</b> уже занят');
				Yii::$app->response->redirect(['site/index']);
			}
			
		}

		$this->attach_anon_order($user);
		
		return $user;
    }
    
    public function attach_anon_order(&$user){
    	if($user->user_type_id == User::TYPE_SERVER)
    		return;
    		
    	if($attach_order = Yii::$app->session->get('attach_order')){
	    	Yii::$app->session->set('attach_order', NULL);
    		$order = Order::findByPk($attach_order);
    		if($order && (-1 == $order->owner_id)){
    			$order->owner_id = $user->id;
    			$order->save();
    			if(!$user->user_type_id){
	    			$user->user_type_id = User::TYPE_CLIENT;
	    			
	    			// автозаполнение пустых частей профиля из полей заказа
	    			if(!$user->fio)$user->fio = $order->fio;
	    			if(!$user->email)$user->email = $order->email;
	    			if(!$user->phone){
	    				$user->phone = $order->phone;
	    				$user->add_verified_phone($user->phone);
	    			}
	    			
					$user->ref_parent = $this->ref_parent;
	    			
	    			$user->save();
    			}
    		}
    		
    	}
    	
    }
    
    public function register(){
    	
		$user = User::findByUsername($this->username);
		if($user){
			Yii::$app->session->setFlash('header-messages', 'Невозможно зарегистрироваться: имя пользователя <b>'.$this->username.'</b> уже занято, придумайте другое или нажмите "Вход"');
			return false;
		}

    	$user = new User();
    	$user->email = $this->username;
    	$user->username = $this->username;
    	$user->password = $this->password;
    	if( ($this->type == User::TYPE_CLIENT) || ($this->type == User::TYPE_SERVER) || ($this->type == User::TYPE_PARTNER))
    		$user->user_type_id = $this->type;

		$user->ref_parent = $this->ref_parent;
		
		//var_dump($user->ref_parent);exit;
		
    	$user->save();

		$this->attach_anon_order($user);

    	if($user->user_type_id)
    		$user->registerGreeting();
    		
    	
    		
    	return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
    }
    
    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {

        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }
        return $this->_user;
    }
}