<?
namespace app\models;

use yii\base\Model;
use yii\helpers\Url;

class Moderation extends Model {
	//property $items; // [title, link, count]
	
	public function getItems(){
		$items = [];
		
		$users = User::find()->where(['moderated'=>0]);
		
		if($users->count())
			$items[] = [
				'title'=>'Пользователей ожидает модерацию:',
				'link' => Url::toRoute(['userlist/index', 'filter_moderated'=>'0']),
				'count' => $users->count()
			];
			
		$tags = Tags::find()->where(['moderated'=>0]);
		
		if($tags->count())
			$items[] = [
				'title'=>'Теги услуг ожидают модерацию:',
				'link' => Url::toRoute(['tags/index', 'filter_moderated'=>'0']),
				'count' => $tags->count()
			];
			
		$portfolio = Portfolio::find()->where(['moderated'=>0]);
		
		if($portfolio->count())
			$items[] = [
				'title'=>'Элементы портфолио ожидают модерацию:',
				'link' => Url::toRoute(['portfolio/moderation']),
				'count' => $portfolio->count()
			];
			
		
		$orders = Order::find()->where(['order_moderated'=>0, 'hidden'=>0]);
		
		if($orders->count())
			$items[] = [
				'title'=>'Проверить заказы:',
				'link' => Url::toRoute(['orders/index', 'Orderlist'=>['filter_moderated'=>'0']]),
				'count' => $orders->count()
			];
			
		$feedback = Order::find()->where(['feedback_moderated'=>0])->andWhere('servitor_id != 0');

		if($feedback->count())
			$items[] = [
				'title'=>'Проверить отзывы:',
				'link' => Url::toRoute(['orders/feedback_moderation']),
				'count' => $feedback->count()
			];
			
		$regions = Regions::find()->where(['moderated'=>0]);

		if($regions->count())
			$items[] = [
				'title'=>'Проверить названия городов:',
				'link' => Url::toRoute(['regions/moderation']),
				'count' => $regions->count()
			];
			
		
		return $items;
	}
}