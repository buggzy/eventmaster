<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use Yii;

class Messages extends ActiveRecord {
	
	public function rules(){
		return [
			['text', 'safe']
		];
	}

	public function attributeLabels(){
		return [
			'text' => 'Сообщение'
		];
	}
	
	public function get_direction(){
		return ($this->owner_id == Yii::$app->user->id);
	}
	
    public static function tableName()
    {
        return 'messages';
    }
    
    public static function GetDealProfiles($me, $order_id){
    	return self::find()->select(['owner_id'])->where(['order_id'=>$order_id, 'recipient'=>$me])->all();
    }    

    public static function GetTreeList($me, $order = null){

		if($order){ // если задан конкретный заказ - выводим данные по нему

			$models = [];

			$count = (new \yii\db\Query)
				->select("count(id)")
				->from("orders")
				->where(['owner_id'=>$me])
				->andWhere(['id'=>$order])->scalar();
				
			if($count){
				$model = new self;
				$model->order_id = $order;
				$models[] = $model;
			}

			$provider = new ArrayDataProvider([
			    'allModels' => $models,
			    'pagination' => [
			    	'pagesize'=>100500]
			]);
			
			
				
		} else { // иначе - выводим данные по всем заказам, по которым была переписка
    	
	    	$query = self::find()->select(['order_id'])
	    		->where(
		        	['or',
			        	['owner_id'=>$me],
			        	['recipient'=>$me],
	        	]
			)
			->andWhere('order_id<>0')
			->distinct();

			$provider = new ActiveDataProvider([
			    'query' => $query,
			    'pagination' => [
			    	'pagesize'=>100500]
			]);
		}
    	
		
		return $provider;
    }

    public static function GetList($me)
    {
    	$me = 0 + $me;
        // Вернуть activedataprovider со списком всех чатов
        $query = self::findBySql(
        	"select order_id, contragent as recipient, max(timestamp) as m from (select order_id, recipient as contragent, timestamp from messages where owner_id=$me union select order_id, owner_id as contragent, timestamp from messages where recipient=$me) as t1 group by order_id, contragent order by m desc");
        
		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		    	'pagesize'=>100500]
		]);
		
		return $provider;
    }
    
    public function get_recipient(){
    	return User::findOne(['id'=>$this->recipient]);
    }

    public function get_order(){
    	return Order::findOne(['id'=>$this->order_id]);
    }
    
    public function get_owner(){
    	return User::findOne(['id'=>$this->owner_id]);
    }

    public function GetIncoming(){
        $me = Yii::$app->user->id;

        $query = self::find()->where(
	        	['owner_id'=>$me]
        );
        
        if($this->order_id)
        	$query = $query->andWhere(['order_id'=>$this->order_id]);
        
        return $query->all();
    	
    }
    
    public function GetOutgoing(){
    	$query = self::find()->
    		where(['recipient'=>$this->recipient, 'owner_id'=>$this->owner_id]);
    		
    	return $query->all();
    }
    
    public function GetMessagesQuery(){
        $me = Yii::$app->user->id;
        $id = $this->recipient;
        
        if($id){
	        $query = self::find()->where(
	        	['or',
		        	['owner_id'=>$me, 'recipient'=>$id],
		        	['owner_id'=>$id, 'recipient'=>$me],
	        	]
	        );
        } else {
	        $query = self::find()->where(
	        	['or',
		        	['owner_id'=>$me],
		        	['recipient'=>$me],
	        	]
	        );
        }
        
		if($this->order_id !== null)
	        $query = $query->andWhere(['order_id'=>$this->order_id]);

		return $query;    	
    }
    
    public function MarkRead(){
        $query = self::find()->where(['recipient'=>Yii::$app->user->id, 'order_id'=>$this->order_id]);
        $ids = [];
		foreach($query->select(['id'])->all() as $id)
			$ids[] = $id->id;

		$queryset = self::updateAll(['unread'=>0], ['id'=>$ids]);

    }


    public function GetMessagesUnread(){
        $query = self::find()->where(['recipient'=>Yii::$app->user->id]);
        if(null !== $this->recipient)
	        $query = $query->andWhere(['owner_id'=>$this->recipient]);
        if(null !== $this->order_id)
	        $query = $query->andWhere(['order_id'=>$this->order_id]);
	        
        $query = $query->andWhere(['unread'=>1]);
        
        return $query;
    	
    }

    public function GetMessagesLast(){
        $query = $this->GetMessagesQuery();
        $query = $query->orderBy(['timestamp'=>SORT_DESC])->one();
        
        return $query;
    	
    }

    
    public function GetMessages()
    {
        // Вернуть activedataprovider со списком всех сообщений одного чата
        
        $query = $this->GetMessagesQuery();
        $query = $query->orderBy(['timestamp'=>SORT_DESC]);
        
		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $provider;

    }
    
}