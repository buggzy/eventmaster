<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\models\User;

class Portfolio extends ActiveRecord {
	
	const TYPE_IMAGE = 1;
	const TYPE_YOUTUBE = 2;
	const TYPE_IFRAME = 3;
	
	public $filter_id;
	
    public static function tableName()
    {
        return 'portfolio';
    }
    
    public function get_owner(){
    	return User::findIdentity($this->owner_id);
    }
    
    public static function findByPk($id){
    	return self::findOne(['id'=>$id]);
    }
    
    private function get_query(){
    	$query = ($this->filter_id == 'unapproved') ? self::find()->where(['moderated'=>0]) : self::find()->where(['owner_id'=>$this->filter_id]);
    	return $query;
    }
    
    public function get_images(){
    	$query = $this->get_query()->andWhere(['type'=>self::TYPE_IMAGE]);
    	return $query->all();
    }
    
    public function get_youtube(){
    	$query = $this->get_query()->andWhere(['type'=>self::TYPE_YOUTUBE]);
    	return $query->all();
    }
    
    public function get_iframe(){
    	$query = $this->get_query()->andWhere(['type'=>self::TYPE_IFRAME]);
    	return $query->all();
    }
    
}