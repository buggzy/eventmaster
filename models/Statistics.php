<?
namespace app\models;

use yii\base\Model;
use yii\helpers\Url;

class Statistics extends Model {
	//property $items; // [title, count]
	
	public function getItems(){
		$items = [];
		
		$day = time() - 60*60*24;
		$users = User::find()->where("created_at > $day")->all();
		$items[] = ['title' => 'Новых регистраций за 24 часа:', 'count' => count($users)];

		$month = time() - 60*60*24*30;
		$users = User::find()->where("created_at > $month")->all();
		$items[] = ['title' => 'Новых регистраций за 30 суток:', 'count' => count($users)];
		
		$subscriptions = PaymentsTypes::find()->all();
		foreach($subscriptions as $sub){
			$transactions = Payments::findAll(['payment_type_id'=>$sub->id, 'status'=>1]);
			if(count($transactions) > 0)
			{
				$items[] = ['title' => $sub->title . ':', 'count' => count($transactions)];
			}
		}


		$subscribers = Payments::findBySql("select count(owner_id) as id from (select owner_id, count(*) as c from payments where status = 1 group by owner_id) as t where c>1")->all();
		$items[] = ['title' => 'Количество исполнителей, приобретшие платную подписку более 1 раза:', 'count' => $subscribers[0]->id];

		$clients = Order::findBySql("select count(owner_id) as id from (select owner_id, count(*) as c from orders group by owner_id) as t where c>1")->all();
		$items[] = ['title' => 'Количество заказчиков, оставивших более 1 заказа:', 'count' => $clients[0]->id];


		return $items;
	}
}