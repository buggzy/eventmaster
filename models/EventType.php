<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\models\User;

class EventType extends ActiveRecord {
    public static function tableName()
    {
        return 'event_types';
    }
    
    public function rules(){
    	return [
    		[['title', 'parent_id'], 'required']
    		];
    }
    
    public static function findByPk($id){
    	return EventType::findOne(['id'=>$id]);
    }

    public function geteventtypes_buttons(){
    	return ''.
    	'<a href="'.Url::toRoute(['eventtype/view', 'id'=>$this->id]).'">Редактировать</a> '.
    		'<a href="'.Url::toRoute(['eventtype/index', 'parent'=>$this->id]).'">Подкатегории</a> '.
    		'<a href="'.Url::toRoute(['eventtype/remove', 'id'=>$this->id]).'">Удалить</a>';
    }
    
    public function GetList($self_id = -1){
    	$retval = ($self_id != 0) ? ['0'=>'Верхний уровень'] : [];
    	foreach(EventType::find()->all() as $i){
    		if($i->id != $self_id)
	    		$retval[$i->id] = $i->title;
    	}
    	return $retval;
    }
    
    public static function GetTreeList(){
    	$data = [[
    		'id'=>-1,
    		'parent_id'=>0,
    		'title'=>'Не выбрано'
    		]
    	];
    	foreach(self::find()->all() as $i){
    		$data[] = ['id'=>$i->id, 'title'=>$i->title, 'parent_id'=>$i->parent_id];
    	}
    	return self::buildTreeRecursive($data, 0, 0);
    }
    
    private static function buildTreeRecursive(&$data, $parent, $level){
    	$retval = [];
    	foreach($data as $i){
    		if($i['parent_id'] == $parent){
    			$id = $i['id'];
    			$titlepad = substr('..........', 0, $level*3) . $i['title'];
				$isNodeVal = self::buildTreeRecursive($data, $id, $level+1);
				if(!empty($isNodeVal))
					$retval[$titlepad] = $isNodeVal;
				else
					$retval[$i['id']] = $i['title'];
    		}
    	}
    	return $retval;
    }


}
