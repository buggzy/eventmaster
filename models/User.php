<?php
namespace app\models;
use Yii;
use yii\data\ActiveDataProvider;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Url;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
	
	const TYPE_ADMIN = 1;
	const TYPE_CLIENT = 2;
	const TYPE_SERVER = 3;
	const TYPE_PARTNER = 4;
	
	const MAX_TAGS = 18;
	
	public $password1, $password2;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

	public static function isThisRole($role){
    	if(Yii::$app->user->getIsGuest())
    		return false;
    		
    	$uid = Yii::$app->user->id;
    	$user = User::findIdentity($uid);
    	return ($user->user_type_id == $role);
		
	}
	
	public static function requireAdminAccess($msg = "Для доступа к разделу требуются права администратора"){
		if(!User::isThisRole(User::TYPE_ADMIN)){
			if($msg)
				Yii::$app->session->setFlash('header-messages', $msg, false);
			return Yii::$app->response->redirect(['user/login', 'backurl'=>Yii::$app->request->getUrl()])->send();
		}
		
		return true;
	}
	
	public static function requireBeUserId($id, $allow_admin = false){
    	$uid = Yii::$app->user->id;
		if($id != $uid){
			if(!$allow_admin || !User::isThisRole(User::TYPE_ADMIN)){
				$user = User::findIdentity($id);
				if($user)
					User::requireAdminAccess("Необходимо выполнить вход под пользователем ".$user->fio);
				else
					User::requireAdminAccess("Необходимо выполнить вход");
					
			}
		}
	}

    public function attributeLabels()
    {
        return [
        	'fio' => 'ФИО / Организация',
        	'phone' => 'Телефон',
        	'region_id' => 'Город',
        	'_region_ids' => 'Город/Города',
        	'_region_title' => 'Город',
        	'user_type' => 'Тип пользователя',
        	'user_type_id' => 'Тип пользователя',
        	'user_type.title' => 'Тип пользователя',
        	'user_type_title' => 'Тип пользователя',
            'username' => 'Имя пользователя (email)',
            'email' => 'Электронная почта',
            'created_at' => 'Дата регистрации',
            'updated_at' => 'Дата изменения',
            'event_type_id' => 'Тип организатора',
            '_event_type_title' => 'Тип организатора',
            'comments' => 'О себе',
            'avatar' => 'Фото или логотип',
            'moderated' => 'Модерация проведена',
            'pay_until' => 'Оплачено до',
            '_paid_now' => 'Оплачен сейчас',
            'password1' => 'Новый пароль',
            'password2' => 'Введите пароль повторно',
            '_tags_multiselect' => 'Предоставляемые услуги',
        ];
    }
    
    public function profileCompleted(){
    	return ($this->fio) && ($this->phone);
    }
    
    public function get_ref_list(){
    	return self::findAll(['ref_parent'=>$this->id]);
    }
    
    public function setRegion(){
    	// TODO - устанавливает регион профиля при регистрации
    	// если получается - устанавливаем соответствующее region id
    }
    
    public function get_verified_phones(){
    	$p = $this->verified_phones;
    	if(null == $p)
    		return [];
    		
    	return preg_split('/;/', $p);
    }

    public function add_verified_phone($phone){
    	$phones = $this->_verified_phones;
    	if(!in_array($phone, $phones)){
    		$phones[] = $phone;
			$this->verified_phones = join(';', $phones);
    	}
    }
    
    public function get_orders(){
    	return Order::findAll(['owner_id'=>$this->id]);
    }
    
    public function get_reviews(){
    	return [
    		'total' => Order::find()->where(['servitor_id'=>$this->id]),
    		'positive' => Order::find()->where(['servitor_id'=>$this->id])->andWhere('feedback_moderated = 0')->andWhere('feedback_rate > 0'),
    		'neutral' => Order::find()->where(['servitor_id'=>$this->id])->andWhere('feedback_moderated = 0')->andWhere('feedback_rate = 0'),
    		'negative' => Order::find()->where(['servitor_id'=>$this->id])->andWhere('feedback_moderated = 0')->andWhere('feedback_rate < 0'),
    		'imported' => ImportedReviews::find()->where(['servitor_id'=>$this->id])
    		];
    }
    
    public function get_reviews_count(){
    	$retval = [];
    	foreach($this->get_reviews() as $key=>$query)
    		$retval[$key] = $query->count();
    		
    	return $retval;
    }
    
    public function get_portfolio_count(){
    	return Portfolio::find()->where(['owner_id'=>$this->id])->count();
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
		return [
			[['email', 'fio', 'password1', 'password2', 'user_type_id', 'event_type_id', 'phone', 'comments', '_tags_multiselect', 'region_id', '_pay_until', 'moderated', '_region_ids'], 'safe'],
			['email', 'email'],
			[['fio', 'email', 'phone', 'event_type_id'], 'required', 'on'=>'server_profile'],
			['_tags_multiselect', 'validate_tags'],
			['event_type_id', 'validate_event_type'],
			['email', 'validate_email'],
			['password1', 'validate_passwords'],
			['password2', 'validate_passwords'],
			['user_type_id', 'validate_user_type'],
			];
    }

    public function validate_event_type($attr){
    	if($this->event_type_id < 0)
	    	$this->addError($attr, 'Нужно обязательно указать тип предоставляемых услуг');
    }
    
    public function validate_tags($attr){
    	if(count($this->$attr)>self::MAX_TAGS)
	    	$this->addError($attr, 'Максимум '.self::MAX_TAGS.' видов услуг');
    }
    
    public function validate_email($attr){
    	if($this->$attr != $this->getOldAttribute($attr)){ 
	    	if(self::find()->where(['username' => $this->email])->count() != 0){
	    		$this->addError($attr, 'Такой адрес уже используется');
	    	}
	    	if(self::find()->where(['email' => $this->email])->count() != 0){
	    		$this->addError($attr, 'Такой адрес уже используется');
	    	}
    	}
    }
    
    public function scenarios(){
    	$scenarios = parent::scenarios();
    	$scenarios['setpass'] = ['password1', 'password2'];
    	$scenarios['register'] = ['username', 'email', 'password', 'fio', 'phone', 'region'];
    	return $scenarios;
    }

    public function validate_passwords($attr){
    	if($this->password1 || $this->password2)
	    	if($this->password1 != $this->password2)
	    		$this->addError('password2', 'Пароли должны совпадать');
	    		
    }
    
    
    public function validate_user_type($attr){
    	
    	if($this->$attr != $this->getOldAttribute($attr)) // если пытаемся изменить роль с неадмина на админа, и при этом мы сами не админ
	    	if( ($this->$attr == User::TYPE_ADMIN) && (!User::isThisRole(User::TYPE_ADMIN)))
	    		$this->addError($attr, 'Назначить себя администратором никак нельзя');

    	return;
    }

    public function uploadAvatarFile($url){
    	// TODO
    }
    
    
    public static function hasReferals(){

		$provider = new ActiveDataProvider([
		    'query' => self::find()
		    	->innerJoin('users u', 'users.id = u.ref_parent'),
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $provider;
    }

    
    public function set_pay_until($val){
    	$this->pay_until = strtotime($val);
    }
    
    public function get_pay_until(){
    	return $this->pay_until ? date('d-m-Y', $this->pay_until) : '';
    }
    
    public function get_paid_now(){
    	return ($this->pay_until >= time());
    }
    
    public function get_fav_ids(){
    	return $this->fav_ids ? preg_split('/;/', $this->fav_ids) : [];
    }
    
    public function get_region_ids(){
    	return $this->region_id ? preg_split('/;/', $this->region_id) : [];
    }

    public function set_region_ids($val){
    	$this->region_id = join(';', Regions::AddNewVals($val));
    }
    
    public function registerGreeting(){
    	
    	$code = 'NEW_USER_NOTYPE';
    	
    	if($this->user_type_id == User::TYPE_CLIENT)
	    	$code = 'NEW_USER_CLIENT';
    	
    	if($this->user_type_id == User::TYPE_SERVER)
	    	$code = 'NEW_USER_SERVER';
	    	
		MailTemplate::sendMail($this->email, $code);

    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
    	return static::findOne(['id' => $id]);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
    
    public function get_event_type_title(){
    	$event = EventType::findByPk($this->event_type_id);
    	return $event ? $event->title : '';
    }
    
    public function getuser_type(){
    	return $this->hasOne(UserTypes::className(), ['id' => 'user_type_id']);
    }

    public function getuser_type_title(){
    	$type = UserTypes::find()->where(['id' => $this->user_type_id])->one();
    	return $type ? $type->title : '';
    }
    
    public function get_region_title(){
    	$region_ids = preg_split('/;/', $this->region_id);
    	$regions = Regions::find()->select(['title'])->where(['id' => $region_ids])->andWhere(['moderated'=>1])->column();
    	if(!$regions)
    		return 'город не указан';
    		
    	$reg_first = array_slice($regions,0, 2);
    	$reg_last = array_slice($regions, 2);
    	
    	if(empty($reg_last))
	    	return join($reg_first, ', ');
	    	
	    return join($reg_first, ', ') . ', ' .
	    '<span class="expand-more-hint">Еще ' . count($reg_last) . '</span>' .
	    '<span class="expand-more">'.join($reg_last, ', ').'</span>';
    }
    
    
    public function set_tags_multiselect($val){
    	$this->taglist_ids = $val ? implode($val,';') : '';
    }

    public function get_tags_multiselect(){
    	return preg_split('/;/', $this->taglist_ids);
    }
    
    public function get_taglist_text(){
    	$taglist = $this->_tags_multiselect;
    	return Tags::find()->where(['id' => $taglist])->all();
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    public static function GetList(){
    	$retval = [];
    	foreach(self::find()->all() as $item)
    		$retval[$item->id] = '(' . $item->id . ') ' . ($item->fio ? $item->fio : 'Не задано');
    	return $retval;
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}