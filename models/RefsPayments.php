<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\models\User;

class RefsPayments extends ActiveRecord {
    public static function tableName()
    {
        return 'ref_payments';
    }
    
    public function attributeLabels(){
    	return [
    		'for_id'=>'Для кого',
    		'amount' => 'Сумма',
    		'timestamp' => 'Дата'
    		];
    }
    
    public function get_for(){
        return $this->hasOne(User::className(), ['id' => 'for_id']);
    }
    
    public function get_owner(){
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
    
    public static function myReferals($id){
    	
		$provider = new ActiveDataProvider([
		    'query' => self::find()->where(['owner_id'=>$id]),
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $provider;
	}
}