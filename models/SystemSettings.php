<?
namespace app\models;

use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use yii\helpers\url;
use Yii;

class SystemSettings extends ActiveRecord {

    public static function tableName()
    {
        return 'settings';
    }

    public static function getVal($key){
    	$record = self::findByPk($key);
    	return $record ? $record->settings_val : NULL;
    }

    public static function setVal($key, $val){
    	$record = self::findByPk($key);
    	if($record){
    		$record->settings_val = $val;
    		$record->save();
    	}
    }

	public function rules()
    {
        return [
            [['settings_key', 'settings_val', 'labels'], 'required']];
    }
	public static function findByPk($key){
    	return self::findOne(['settings_key'=>$key]);
	}

    public static function GetListArray(){
    	$retval = [];
    	foreach(self::find()->all() as $i){
    		$retval[$i->id] = $i->title;
    	}
    	return $retval;
    }

    public function get_provider(){

		$query = self::find();
		
		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);

		return $provider;
    	
    }
	
}

?>