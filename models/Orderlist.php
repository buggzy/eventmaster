<?php
namespace app\models;

use yii\base\Model;

class Orderlist extends Model {
	public $filter_moderated;
	public $filter_date;
	public $filter_type;
	public $filter_city;
	public $id;
	
	public function rules(){
		return [
			[['filter_moderated', 'filter_date', 'filter_type', 'filter_city'], 'safe']
			];
	}
	
	public function attributeLabels(){
		return [
			'filter_moderated' => 'Прошедшие модерацию?',
			'filter_date' => 'Фильтр по дате',
			'filter_type' => 'Тип заказа',
			'filter_city' => 'Город'
			];
	}
	
	public function alterQuery(&$query){
		
		$query = $query->andWhere(['hidden'=>0]);
		
		if(NULL != $this->id)
			$query = $query->andWhere(['id'=>$this->id]);
		
		if(NULL != $this->filter_moderated)
			$query = $query->andWhere(['order_moderated'=>$this->filter_moderated]);

		if((NULL != $this->filter_type) &&(0 != $this->filter_type))
			$query = $query->andWhere(['event_type_id'=>$this->filter_type]);

		if((NULL != $this->filter_city) &&(0 != $this->filter_city))
			$query = $query->andWhere(['region'=>$this->filter_city]);

		if(NULL != $this->filter_date){
			$time = time();
			switch($this->filter_date) {
				case -1 : $query = $query->andWhere("unix_timestamp(str_to_date(order_date, '%d-%m-%Y')) < $time"); break;
				case 1 : $query = $query->andWhere("unix_timestamp(str_to_date(order_date, '%d-%m-%Y')) >= $time"); break;
			}
			
		}
			

	}
}