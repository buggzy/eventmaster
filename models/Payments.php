<?

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;


class Payments extends ActiveRecord {
	
	public static function findByPk($id){
		return self::findOne(['id'=>$id]);
	}
	
    public static function tableName()
    {
        return 'payments';
    }
    
    public function rules(){
    	return [
    		[['payment_type_id', 'owner_id', 'status'], 'safe']
    	];
    }

    public function scenarios(){
    	return [
    		'default' => ['payment_type_id'],
    		'admin' => ['payment_type_id', 'owner_id', 'status']
    	];
    }

    public function attributeLabels(){
    	return [
    		'payment_type_id' => 'Выберите тип подписки'
    	];
    }
    
    public function get_payment_type(){
    	return PaymentsTypes::find()->where(['id'=>$this->payment_type_id])->one();
    }
    
	public function getDataProvider(){
		
		$query = self::find()->orderBy(['id'=>SORT_DESC]);
			
		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $provider;
		
	}
	
	public static function GetList(){
		return self::find()->all();
	}

}
