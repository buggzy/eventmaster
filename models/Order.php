<?php
namespace app\models;

use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\models\User;
use app\models\EventType;
use Yii;

class Order extends ActiveRecord {
    public static function tableName()
    {
        return 'orders';
    }
    
    public function rules(){
    	return [
    		[['title', 'event_type_id', 'order_date', 'region', 'fio', 'email', '_tags_multiselect'], 'required'],
    		[['offers'], 'integer'],
    		[['phone_add'], 'verify_phone'],
    		[['persons', 'comment', 'feedback_text', 'feedback_rate', 'order_moderated', 'budget', 'fio', 'phone', 'phone_verify', 'phone_show', 'social'], 'safe']
		];
    }
    
    public function scenarios(){
    	$scenarios = parent::scenarios();
    	$scenarios['feedback'] = ['feedback_rate', 'feedback_text'];
    	$scenarios['final_letter'] = ['final_letter'];
    	$scenarios['cron_daily'] = ['subscribers_emailed'];
    	return $scenarios;
    }
    
    public $phone_verify, $phone_add;
    
    public function attributeLabels()
    {
        return [
        	'title' => 'Название заказа',
        	'event_type_id' => 'Тип заказа',
        	'_event_type.title' => 'Тип заказа',
        	'order_date' => 'Дата мероприятия',
        	'budget' => 'Бюджет',
        	'social' => 'Связаться с Вами через социальную сеть?',
        	'persons' => 'Количество человек',
        	'region' => 'Область, город, район',
        	'offers' => 'Получать предложений',
        	'comment' => 'Комментарий',
        	'edited' => 'Редакций',
        	'fio' => 'Имя контактного лица',
        	'phone' => 'Контактный телефон',
        	'phone_add' => 'Контактный телефон',
        	'phone_verify' => 'Код SMS',
        	'phone_show' => 'Показывать телефон на сайте',
        	'_visible' => 'Виден',
        	'email' => 'Электронная почта',
        	'_tags_multiselect' => 'Необходимые услуги',
        	'feedback_rate' => 'Насколько Вам понравилось сотрудничество?',
            '_orderlist_buttons' => ''
        ];
    }


    public function beforeSave($insert){
    	
		if($this->phone_add){
			$this->phone = $this->phone_add;
			if($this->owner_id > 0){
				$user = $this->owner;
				$user->add_verified_phone($this->phone);
				$user->save();
			}
		}
    	
    	return parent::beforeSave($insert);
    }
    
    public static function findByPk($id){
    	return self::findOne(['id'=>$id]);
    }
    
    public function getphonevalidation(){
    	return substr(md5("a" . $this->phone_add), 2, 4);
    }
    
    public function beforeValidate(){

    	if(!$this->phone && !$this->phone_add){
    		$this->addError('phone', 'Необходимо ввести телефон');
    		$this->addError('phone_add', 'Необходимо ввести телефон');
    		return false;
    	}
    	
    	return parent::beforeValidate();
    }
    
    public function verify_phone($attr){
    	if($this->$attr){
    		if(!in_array($this->phone_add, $this->verifiedPhones()) && $this->phone_verify != $this->phonevalidation){
    			$this->addError($attr, 'Введите код из SMS');
    			return false;
    		}
    	}

    }
    
    public function obtainvericode(){

    	$sms_remains = 0 + SystemSettings::getVal('sms-verify');
    	if($sms_remains > 0){
    		Yii::$app->sms->send_sms($this->phone_add, "Ваш код для проверки телефона: ".$this->phonevalidation);
    		SystemSettings::setVal('sms-verify', $sms_remains-1);
    	}
    }

    
	public function verifiedPhones(){
    	return ($this->owner_id > 0) ? $this->owner->_verified_phones : [];
	}
    
    public static function getOrders($uid){
    	return self::find()->where(['owner_id'=>$uid])->all();
    }
    
    /*  Делал ли текущий юзер предложение по этому заказу */
    public function get_contacted(){
    	if(Yii::$app->user->getIsGuest())
    		return false;
    		
    	$user_id = Yii::$app->user->id;
    	$messages = Messages::find()->where(['owner_id'=>$user_id, 'order_id'=>$this->id]);
    	return (0 != $messages->count());
    }

    public function get_contragent_count(){

    	$contragents = Messages::find()->select(['owner_id'])->where(['order_id'=>$this->id])->distinct();
    	return $contragents->count();
    }
    
    public static function getFeedbackModeration(){
		
		$query = self::find()->where(['feedback_moderated'=>0])->andWhere('servitor_id <> 0');

		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $provider;
    }
    // видим ли заказ?
    public function get_visible(){
    	
    	$date = strtotime($this->order_date);
    	if(time() > $date)
	  		return false;
    		
    	if($this->auto_hide && (time() > $this->auto_hide))
    		return false;
    		
    	return true;
    }
    
    public function setAutoHide(){

    	$this->auto_hide = time() + SystemSettings::getVal('autohide_time');
    	$this->autohide_notified = 0;
    }

    public function resetAutoHide(){

    	$this->auto_hide = null;
    	$this->autohide_notified = 0;
    }
    
    public function get_tags_multiselect(){
    	return preg_split('/;/', $this->tags_id);
    }
    
    public function set_tags_multiselect($val){
		$val = Tags::AddNewTags($val);
    	$this->tags_id = implode($val,';');
    }
    
    public function get_taglist_text(){
    	$taglist = $this->_tags_multiselect;
    	return Tags::find()->where(['id' => $taglist])->all();
    }

	public function get_project_past(){
		$date = strtotime($this->order_date);
		return (time() >= $date);
	}
    
    public function getowner(){
    	$user = User::findIdentity($this->owner_id);
    	return $user;
    }
    public function get_event_type(){
    	$user = EventType::findByPk($this->event_type_id);
    	return $user;
    }
    public function get_region(){
    	$region = Regions::findByPk($this->region);
    	return $region ? $region->title : '';
    }
    public function get_owner_profile(){
    	$user = $this->getowner();
    	return $user ? '<a href="'.Url::toRoute(['user/view', 'id'=>$this->owner_id]).'">'.$user->fio.'</a>' : '';
    }
    public function get_orderlist_buttons(){
    	return ''.
    	'<a href="'.Url::toRoute(['orders/view', 'id'=>$this->id]).'">Редактировать</a> '.
    	'<a href="'.Url::toRoute(['orders/remove', 'id'=>$this->id]).'">Удалить</a>';
    }
}
