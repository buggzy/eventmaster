<?php
namespace app\models;

use yii\db\ActiveRecord;


class ImportedReviews extends ActiveRecord {
    public static function tableName()
    {
        return 'imported_reviews';
    }
    
    public function rules(){
    	return [
    		[['servitor_id', 'name', 'text', 'created', '_created'], 'safe']
    		];
    }

    public function attributeLabels(){
    	return [
    			'servitor_id' => 'Исполнитель',
    			'name' => 'Имя оставившего отзыв',
    			'text' => 'Текст',
    			'_created' => 'Дата отзыва'
    		];
    }
    
    public static function findByPk($id){
    	return self::findOne(['id'=>$id]);
    }
    
    public function get_servitor(){
    	return $this->hasOne(User::className(), ['id'=>'servitor_id']);
    }
    
    public function get_created(){
    	return date('d-m-Y', $this->created);
    }
    
    public function set_created($val){
    	$this->created = strtotime($val);
    }
    
}
