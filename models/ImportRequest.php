<?php
namespace app\models;

use yii\base\Model;
use app\models\User;

class ImportRequest extends Model {
	public $text;
	
	public function rules(){
		return [['text', 'safe']];
	}
	
	public function attributeLabels(){
		return [
			'text'=>"Введите точные адреса страниц, с которых импортировать Ваши отзывы"];
	}
}