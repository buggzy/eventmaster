<?
namespace app\models;

use yii\db\ActiveRecord;

class UserTypes extends ActiveRecord {
    public static function tableName()
    {
        return 'user_types';
    }
    public static function GetList($exclude = -1){
    	$retval = [];
    	$retval[''] = '(не задан)';
    	foreach(UserTypes::find()->all() as $i){
    		if($i->id != $exclude)
	    		$retval[$i->id] = $i->title;
    	}
    	
    	return $retval;
    }
}