<?
namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\url;
use Yii;
use yii\data\ActiveDataProvider;


class Tags extends ActiveRecord {
	
    public static function tableName()
    {
        return 'tags';
    }
    
    public function attributeLabels(){
    	return [
            'moderated' => 'Модерация проведена',
            'title' => 'Тег'
    	];
    }
    
    public function rules(){
    	return [
    		[['title', 'moderated'], 'safe']
    		];
    }
    
    public function get_provider(){
		$query = Tags::find();
		
		$filter_moderated = Yii::$app->getRequest()->getQueryParam('filter_moderated');
		
		if($filter_moderated !== NULL)
			$query = $query->andWhere(['moderated'=>$filter_moderated]);
		
		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);

		return $provider;
    	
    }
    
    public static function AddNewTags($tags){
    	
		$retval = [];
    	if(is_array($tags))foreach($tags as $title){
    		$t = self::find()->where(['or', ['title'=>$title], ['id'=>$title]])->one();
    		if($t){
    			$retval[] = $t->id;
    			continue;
    		}
    		$t = new self();
    		$t->title = $title;
    		$t->insert();
    		$retval[] = $t->id;
    	}
    	
    	return $retval;

    }

    
    public static function findByPk($id){
    	return Tags::findOne(['id'=>$id]);
    }

    public static function GetList(){
    	$retval = [];
    	foreach(Tags::find()->all() as $i){
    		$retval[$i->id] = $i->title;
    	}
    	return $retval;
    }
    
    public function get_tags_buttons(){
    	return '<a href="'.Url::toRoute(['tags/view', 'id'=>$this->id]).'">Редактировать</a>';
    }

}