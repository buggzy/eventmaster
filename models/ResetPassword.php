<?

namespace app\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ResetPassword extends Model {
	
	public $username;
	
	public function attributeLabels(){
		return [
			'username' => 'Введите адрес почты'
			];
	}
	
	public function rules(){
		return [
			[['username'], 'safe']
		];
	}
	
}