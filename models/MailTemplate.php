<?
namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\url;
use Yii;
use Html2Text\Html2Text;


class MailTemplate extends ActiveRecord {
    public static function tableName()
    {
        return 'mail_templates';
    }
    
    public function rules(){
    	return [
    		[['code','title','template_text', 'comment'], 'safe']
    		];
    }

    
    public static function findByPk($id){
    	return self::findOne(['id'=>$id]);
    }

    public static function GetList(){
    	$retval = [];
    	foreach(self::find()->all() as $i){
    		$retval[$i->id] = $i->title;
    	}
    	return $retval;
    }
    
    public function get_templateslist_buttons(){
    	return '<a href="'.Url::toRoute(['mailtemplates/view', 'id'=>$this->id]).'">Редактировать</a>';
    }
    
    public static function sendMail($to, $code, $args = []){
    	$template = self::find()->where(['code'=>$code])->one();
    	if(!$template)
    		return false;

		$template_text = $template->template_text;
		foreach($args as $k=>$v){
			$template_text = preg_replace("/#$k#/", $v, $template_text);
		}    		
        
        $template_text_plain =(new Html2Text($template_text))->getText();

    	$message = Yii::$app->mailer->compose();

    	return 
    		$message -> setFrom('info@eventblaster.ru')
		    	->setTo($to)
		    	->setSubject($template->title)
    	    	->setHtmlBody($template_text)
    	    	->setTextBody($template_text_plain)
		    	->send();
    }

}