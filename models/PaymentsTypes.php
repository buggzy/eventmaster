<?

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;


class PaymentsTypes extends ActiveRecord {
	
	public static function findByPk($id){
		return self::findOne(['id'=>$id]);
	}
	
    public static function tableName()
    {
        return 'payments_types';
    }
    
    public function rules(){
    	return [
    		[['title', 'period', 'cost'], 'required'],
    		[['description'], 'safe']
    	];
    }
    
	public function getDataProvider(){
		
		$query = self::find();
			
		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $provider;
		
	}
	
	public static function GetList(){
		return self::find()->all();
	}

	public static function GetArrayList(){
		$retval = [];
		foreach(self::GetList() as $i){
			$retval[$i->id] = $i->title;
		}
		return $retval;
	}

}
