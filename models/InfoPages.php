<?

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;


class InfoPages extends ActiveRecord {
	
	public static function findByPk($id){
		return self::findOne(['id'=>$id]);
	}
	
    public static function tableName()
    {
        return 'info_pages';
    }
    
    public function rules(){
    	return [
    		[['title', 'content', 'url'], 'required']
    	];
    }
    
	public function getDataProvider(){
		
		$query = self::find();
			
		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $provider;
		
	}
	
	public static function GetList(){
		return self::find()->all();
	}

	public static function findByUrl($url){
		return self::find()->where(['url'=>$url])->one();
	}

}
