<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\models\User;

class Regions extends ActiveRecord {
    public static function tableName()
    {
        return 'regions';
    }
    
    public function rules(){
    	return [
    		[['title'], 'required']
    		];
    }
    
    public static function findByPk($id){
    	return self::findOne(['id'=>$id]);
    }

    public static function findByName($title){
    	return self::find()->where(['title'=>$title])->one();
    }

    public static function GetList($self_id = -1){
    	$retval = ($self_id != 0) ? ['0'=>'Верхний уровень'] : [];
    	foreach(self::find()->where(['moderated'=>1])->orderBy('title')->all() as $i){
    		if($i->id != $self_id)
	    		$retval[$i->id] = $i->title;
    	}
    	return $retval;
    }
    
    public static function AddNewVals($vals){
    	
		$retval = [];
    	if(is_array($vals))foreach($vals as $title){
    		$t = self::find()->where(['or', ['title'=>$title], ['id'=>$title]])->one();
    		if($t){
    			$retval[] = $t->id;
    			continue;
    		}
    		$t = new self();
    		$t->title = $title;
    		$t->insert();
    		$retval[] = $t->id;
    	}
    	
    	return $retval;

    }


}
