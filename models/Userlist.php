<?

namespace app\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class Userlist extends Model {
	
	public $region;
	public $sortby;
	public $type;
	public $byname;
	public $favs;
	
	public function getSortTypes(){
		return [
			'rating' => 'рейтингу',
			'alphabet' => 'алфавиту'
		];
	}
	
	public function attributeLabels(){
		return [
			'sortby' => 'Сортировать по:',
			'region' => 'Город:',
			'type' => 'Тип',
			'byname' => 'Искать по имени'
 			];
	}
	
	public function rules(){
		return [
			[['sortby', 'region', 'type', 'byname'], 'safe']
			];
	}
	
	public function getDataProvider(){
		
		if (Yii::$app->user->getIsGuest() || User::isThisRole(User::TYPE_CLIENT))
			$query = User::find()->where(['user_type_id'=>User::TYPE_SERVER]);
		elseif (User::isThisRole(User::TYPE_SERVER))
			$query = User::find()->where(['user_type_id'=>User::TYPE_CLIENT]);
		else
			$query = User::find();
			
		if($region = $this->region){
			$query->andWhere("concat(';', concat(region_id, ';')) like '%;$region;%'");
		}

		if($this->type){
			$query->andFilterWhere(['users.event_type_id'=>$this->type]);
		}

		$filter_moderated = Yii::$app->getRequest()->getQueryParam('filter_moderated');

		if($filter_moderated !== NULL)
			$query = $query->andWhere(['moderated'=>$filter_moderated]);
			
		if(!User::isThisRole(User::TYPE_ADMIN))
			$query = $query->andWhere(['moderated'=>1]);

		if($this->byname)
			$query = $query->andWhere(['like', 'users.fio', $this->byname]);

		if($this->sortby == 'alphabet')
			$query = $query->orderBy('fio ASC');

		if($this->sortby == 'rating')
			$query = $query->select(['users.*', "sum(if(feedback_rate > 0, 1, 0)) as cnt"])
				->groupBy('users.id')
				->leftJoin('orders', 'orders.servitor_id = users.id')
				->orderBy('cnt DESC');


		if(!Yii::$app->user->getIsGuest() && $this->favs){
			$filter_favs = Yii::$app->user->identity->_fav_ids;
			if(empty($filter_favs))
				$filter_favs = ['-9999'];
				
			$query->andFilterWhere(['id'=>$filter_favs]);
		}


		$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ]
		]);
		
		return $provider;
		
	}	
}



?>