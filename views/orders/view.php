<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\EventType;
use app\models\Regions;
use app\models\Tags;
use app\models\User;
use kartik\select2\Select2;
use kartik\date\DatePicker;

$this->title = $model->isNewRecord ? "Создание заказа" : "Редактирование заказа";

?>
<div class="inner-section clearfix hilite-required">
	<h2 class="nomt"><? echo $this->title?></h2>
<? $form = ActiveForm::Begin(); ?>
<? if(User::isThisRole(User::TYPE_ADMIN)) {
	echo $form->field($model, 'order_moderated')->dropDownList(
		['0'=>'Нет', '1'=>'Да']
	);
} ?>

<div class="row">
	<div class="col-sm-4">
		<? echo $form->field($model, 'title')->textInput(); ?>
	</div>
	<div class="col-sm-4">
		<? echo $form->field($model, 'event_type_id')->dropDownList(
			EventType::GetTreeList()
		); ?>
	</div>
	<div class="col-sm-4">
		<?
		echo $form->field($model, '_tags_multiselect')->widget(Select2::classname(), [
		    'data' => Tags::GetList(),
		    'language' => 'ru',
		    'options' => [
		    	'placeholder' => 'Выберите услуги',
		    ],
		    'pluginOptions' => [
				'tags' => true,
		    	'multiple' => true,
		        'allowClear' => true
		    ],
		]);
		?>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<? echo $form->field($model, 'order_date')->widget(DatePicker::classname(), [
				'language' => 'ru',
				'removeButton' => false,
			    'pluginOptions' => [
			        'autoclose'=>true,
			        'format' => 'dd-mm-yyyy'
			    ]
		]); ?>
	</div>
	<div class="col-sm-4">
		<? echo $form->field($model, 'budget')->textInput(['placeholder'=>'по договоренности']); ?>
	</div>
	<div class="col-sm-4">
		<? echo $form->field($model, 'persons')->textInput(); ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<?
				$veryphones = [];
				foreach($model->verifiedPhones() as $p)
					$veryphones[$p] = $p;
		?>
		<div class="order-phones <? if($model->phone_add || empty($veryphones))echo "new-phone"?>">
			<div class="phone-add">
				<div class="row">
					<div class="col-sm-6">
						<? echo $form->field($model, 'phone_add')->widget(\yii\widgets\MaskedInput::className(),[
								    'mask' => '+7 (999) 999-99-99',
								]) ?>
					</div>
					<div class="col-sm-3">
						<? echo $form->field($model, 'phone_verify')->widget(\yii\widgets\MaskedInput::className(),[
							    'mask' => '****',
							]) ?>
					</div>
					<div class="col-sm-3">
						<label>Получить</label>
							<button type="button" class="get-sms-code btn">Запросить</button>
					</div>
				</div>
			</div>
			<div class="select-phone">
				<?
					
				echo $form->field($model, 'phone')->widget(Select2::classname(), [
				    'data' => $veryphones,
				    'language' => 'ru',
				    'options' => [
				    	'placeholder' => 'Выберите телефон из списка или добавьте новый',
				    ],
				    'pluginOptions' => [
						'tags' => false,
				    	'multiple' => false,
				        'allowClear' => true
				    ],
				    'addon' => [
				    	'append' => ['content' => '<button type="button" class="add_phone">Новый телефон</button>']
				    ]
				]);?>
			</div>
		</div>
		<? echo $form->field($model, 'phone_show')->checkBox(); ?>
	</div>
	<div class="col-sm-4">
		<? echo $form->field($model, 'fio')->textInput(); ?>
	</div>
	<div class="col-sm-4">
		<? echo $form->field($model, 'email')->textInput(); ?>
	</div>
</div>

<? echo $form->field($model, 'region')->widget(Select2::classname(), [
    'data' => Regions::GetList(0),
    'language' => 'ru',
    'options' => [
    	'placeholder' => 'Выберите город',
    ],
    'pluginOptions' => [
		'tags' => false,
    	'multiple' => false,
        'allowClear' => true
    ],
]);
?>

<? echo $form->field($model, 'social')->textInput(['class'=>$model->social ? '' : 'not-show']); ?>


<? echo $form->field($model, 'comment')->textArea(); ?>


<?= Html::submitButton('Разместить заказ', ['name'=>'save', 'class' => 'btn btn-primary']) ?>

<? ActiveForm::end(); ?>
</div>