<?
use yii\helpers\Url;
use yii\helpers\BaseHtml;

?>
<div class="orderlist-item">
	<div class="hdr">
		<span class="title"><? echo BaseHtml::encode($model->title) ?></span>
		<span class="region">(<? echo BaseHtml::encode($model->_region) ?>)</span>
	</div>
	<hr>
	<div class="row">
		<div class="col-lg-4 mb">
			<div class="preview">
				<div class="date">
					<i class="fa fa-calendar"></i> Дата проведения: <span class="val"><? echo BaseHtml::encode($model->order_date); ?></span>
				</div>
				<? if($model->auto_hide) { ?>
				<div class="autohide">
					<i class="fa fa-spinner"></i> Будет автоматически скрыт: <span class="val"><? echo date('d-m-Y H:i', $model->auto_hide); ?></span>
				</div>
				<? } ?>
				<div class="type">
					<i class="fa fa-sticky-note-o"></i> Что нужно: <span class="val"><? echo $model->_event_type ? BaseHtml::encode($model->_event_type->title) : ''; ?></span>
				</div>
				<div class="type">
					Опубликован \ скрыт: <span class="val"><? echo $model->_visible ? 'Опубликован' : 'Снят с публикации'; ?></span>
				</div>
                <? if($model->persons) { ?>
                <div class="type">
    				<i class="fa fa-users"></i> Количество персон: <? echo BaseHtml::encode($model->persons); ?>
				</div>
                <? } ?>

        		<div class="type">
					<i class="fa fa-money"></i> Бюджет: <? echo BaseHtml::encode($model->budget ? $model->budget : 'По договоренности'); ?>
				</div>
				<? if($model->tags_id) { ?>
				<div class="services">
					<div class="taglist">
					Услуги:
						<ul>
						<? foreach($model->_taglist_text as $i)
							echo '<li><i class="fa fa-tag"></i> ' . BaseHtml::encode($i->title) . "</li> "
						?>
						</ul>
					</div>
				</div>
				<? } ?>
			</div>
		</div>
		<div class="col-lg-8 mb">
			<? if($model->comment) { ?>
			<div class="resume">
				<label>Описание проекта:</label>
				<div class="comment-text"><? echo BaseHtml::encode($model->comment) ?></div>
			</div>
			<? } ?>
		</div>
	</div>
	<div class="buttons">
		<a class="btn" href="<? echo Url::toRoute(['orders/view', 'id'=>$model->id])?>"><i class="fa fa-edit"></i> Редактировать</a>
		<a class="btn" onclick="return confirm('Уверены, что хотите удалить заказ?');" href="<? echo Url::toRoute(['orders/remove', 'id'=>$model->id])?>"><i class="fa fa-close"></i> Удалить</a>
	</div>



</div>