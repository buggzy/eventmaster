<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\EventType;
use app\models\Regions;
use app\models\Tags;
use kartik\select2\Select2;
use kartik\date\DatePicker;

?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<h2>Отзыв</h2>
<div class="trow">
	<b>Название заказа:</b> <? echo $order->title ?>
</div>
<div class="trow">
	<b>Исполнитеем заказа был:</b> <? echo $user->fio ?>
</div>
<? echo $form->field($order, 'feedback_rate')->dropDownList(
	[
		'-2'=>'Очень плохо',
		'-1'=>'Плохо',
		'0' => 'Нормально',
		'1' => 'Хорошо',
		'2' => 'Отлично!'
		]
	); ?>
<? echo $form->field($order, 'feedback_text', ['template'=>'<label>Поделитесь впечатлениями о сотрудничестве</label>{input}'])->textArea(); ?>


<?= Html::submitButton('Подтверждаю', ['name'=>'save', 'class' => 'btn btn-primary']) ?>&nbsp;
<?= Html::submitButton('Отменить', ['name'=>'cancel', 'class' => 'btn btn-primary', 'onclick'=>'history.back(); return false;']) ?>

<? ActiveForm::end(); ?>
</div>