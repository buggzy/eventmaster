<?
use yii\widgets\ListView;
use app\models\User;
use yii\helpers\Url;

$this->title = 'Ваши заказы';

?>

<? if($dataProvider->totalCount == 0) { ?>
<div class="inner-section clearfix">
	<div class="row">
		<div class="col-sm-7">
			<h2><? echo $this->title?></h2>
			<p>
				<a href="<? echo Url::toRoute(['orders/view', 'id'=>'new']); ?>" class="btn btn-default"><i class="fa fa-plus"></i> Добавить заказ</a>
			</p>
			<p>
				Добавьте заказ с описанием мероприятия, чтобы получать предложения.
			</p>
		</div>
		<div class="col-sm-5 hidden-xs text-center">
			<? echo $this->render('@app/views/site/greetings') ?>
		</div>
	</div>
</div>
<? } else { ?>

<div class="bars-orderlist-section clearfix">
	<div class="inner">
		<div class="top-action-buttons">
			<a class="btn" href="<? echo Url::toRoute(['orders/view', 'id'=>'new'])?>"><i class="fa fa-plus"></i> Добавить заказ</a>
		</div>
		<?= ListView::widget([
		    'dataProvider' => $dataProvider,
		    'options' => [
		        'tag' => 'div',
		        'class' => 'list-wrapper',
		        'id' => 'list-wrapper',
		    ],
		    'itemView' => '_list_item_client'
		]) ?>

	</div>
</div>
<? } ?>

