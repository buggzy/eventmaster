<?
use yii\widgets\ListView;
use app\models\User;
use app\models\Regions;
use app\models\EventType;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;

$this->title = isset($title) ? $title : "Заказы";

?>
<div class="bars-userlist-section clearfix">
	<div class="inner">
		<? if($searchModel && (null == $searchModel->id)) { ?>
		<div class="cutted-form">
			<? $form = ActiveForm::Begin(); ?>
			<div class="row">
				<div class="col-sm-6">
					<?
					$types = EventType::GetTreeList(); array_unshift($types, 'Любой');
					echo $form->field($searchModel, 'filter_type')->dropDownList($types); ?>
				</div>
				<div class="col-sm-6">
					<?
					$regions = Regions::GetList();
					echo $form->field($searchModel, 'filter_city')->widget(Select2::classname(), [
					    'data' => $regions,
					    'language' => 'ru',
					    'options' => [
					    	'placeholder' => 'Выберите город',
					    ],
					    'pluginOptions' => [
							'tags' => false,
					    	'multiple' => false,
					        'allowClear' => true
					    ],
					]);
					?>
				</div>
			</div>
			<div class="text-center">
				<button class="btn btn-default wide-btn">Фильтровать</button>
			</div>
			<? ActiveForm::End(); ?>
		</div>
		<? } ?>
		<? if($dataProvider->totalCount == 0) { ?>
		<div class="inner-section">
			<div class="row">
				<div class="col-sm-7">
					<h2><? echo $this->title?></h2>
						<? echo isset($notfound) ? $notfound : 'По запросу ничего не найдено'; ?>
					</p>
				</div>
				<div class="col-sm-5 hidden-xs text-center">
					<? echo $this->render('@app/views/site/greetings') ?>
				</div>
			</div>
		</div>
		<? } else { ?>

		<?= ListView::widget([
		    'dataProvider' => $dataProvider,
		    'options' => [
		        'tag' => 'div',
		        'class' => 'list-wrapper',
		        'id' => 'list-wrapper',
		    ],
		    'itemView' => '_list_item_server',
		    'viewParams' => ['single'=>($searchModel  !== false) && (null != $searchModel->id)],
		    'summary' => (!$searchModel) || (null == $searchModel->id) ? null : ''
		]) ?>
		<? } ?>

	</div>
</div>
