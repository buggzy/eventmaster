<?
use yii\helpers\Url;
use yii\helpers\BaseHtml;

if($single){

			$description = 'Город: '.$model->_region . ', дата: '.$model->order_date . ', бюджет: '.($model->budget ? $model->budget : 'по договоренности');

			\Yii::$app->view->registerMetaTag([
				'name'=>'description',
				'content'=>strip_tags($description),
			], 'meta_description');
			
			$this->title = 'Заказ: '. $model->title;
}

?>
<div class="orderlist-item">
	<div class="row">
		<div class="col-sm-3">
			<div class="hdr text-center mb">
				<span class="rounded-greetings"><? echo BaseHtml::encode($model->title) ?><span class="triangle"></span></span>
				<br>
				<img src="/img/p3.png" alt="">
			</div>
		</div>
		<div class="col-sm-9">
			<div class="preview">
				<div class="pull-right permalink">
					<? if(!$single) { ?>
					<a href="<?=Url::toRoute(['orders/index', 'id'=>$model->id])?>">Постоянная ссылка</a>
					<? } ?>
				</div>
				<div class="region">
					<i class="fa fa-map-marker"></i> <? echo BaseHtml::encode($model->_region) ?>
				</div>
				<div class="date">
					<i class="fa fa-calendar"></i> Дата проведения: <? echo BaseHtml::encode($model->order_date); ?>
				</div>
    			<div class="type">
					<i class="fa fa-check"></i> Что нужно: <? echo BaseHtml::encode($model->_event_type->title); ?>
				</div>
                <? if($model->persons) { ?>
                <div class="type">
					<i class="fa fa-users"></i> Количество персон: <? echo BaseHtml::encode($model->persons); ?>
				</div>
                <? } ?>
                <div class="type">
					<i class="fa fa-money"></i> Бюджет: <? echo BaseHtml::encode($model->budget ? $model->budget : 'По договоренности'); ?>
				</div>
				<? if($model->tags_id) { ?>
				<div class="services mb">
					<div class="taglist">
					Услуги:
						<ul>
						<? foreach($model->_taglist_text as $i)
							echo "<li>" . BaseHtml::encode($i->title) . "</li>"
						?>
						</ul>
					</div>
				</div>
				<? } ?>
				<? if($model->comment) { ?>
				<div class="resume mb">
					Комментарий
					<div class="comment-text"><? echo BaseHtml::encode($model->comment) ?></div>
				</div>
				<? } ?>
				<div class="contacts mb">
					<? $contacts = ['общение через Eventblaster'];
					if($model->phone && $model->phone_show)
						$contacts[] = 'контактный телефон';
					if($model->email)
						$contacts[] = 'электронная почта';
					if($model->social)
						$contacts[] = 'социальные сети';
						
					$contacts = join(', ', $contacts);
					?>
					Доступны контакты для связи: <?=$contacts?>
				</div>
				<div class="buttons">
					<? if(!Yii::$app->user->getIsGuest()) {
						$in_favs = !Yii::$app->user->getIsGuest() && in_array($model->id, Yii::$app->user->identity->_fav_ids) ? 'in-favs' : '';
					?>
					<a href="<? echo Url::toRoute(['user/fav_toggle', 'id'=>$model->id ])?>" class="ajax-link favs-toggle btn btn-primary <?=$in_favs?>">
						<span class="off">В избранное</span>
						<span class="on">Добавлено в избранное</span>
					</a>
					<? } ?>
					<? if($model->_contacted) { ?>
					<a class="btn btn-primary" href="<? echo Url::toRoute(['message/deal', 'order'=>$model->id])?>">Открыть переписку</a>
					<? } else { ?>
					<a class="btn btn-primary" href="<? echo Url::toRoute(['message/deal', 'order'=>$model->id])?>">Сделать предложение</a>
					<? } ?>
					<div class="contragent-count">
						По этому заказу уже сделали предложение: <? echo $model->_contragent_count ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
		


</div>