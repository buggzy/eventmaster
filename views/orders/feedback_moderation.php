<?
use yii\grid\GridView;
use app\models\User;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
<?

$columns = [
        'id',
        'title',
        '_event_type.title',
        'feedback_text',
        'order_date',
		'_owner_profile:raw',
		[
        	'content'=>function($model){
        		return '<a href="'.Url::toRoute(['orders/feedback_approve', 'id'=>$model->id]).'">Утвердить</a> '.
        		'<a href="'.Url::toRoute(['orders/feedback_cancel', 'id'=>$model->id]).'">Удалить</a> ';
        	},
        	'label' => 'Утвердить'
    	]
    ];
    



if(User::isThisRole(User::TYPE_CLIENT)) { ?>
<a href="<? echo Url::toRoute(['orders/view', 'id'=>'new'])?>">Добавить</a>
<? } ?>


<?= GridView::widget([
    'dataProvider' => $model->getFeedbackModeration(),
    'columns' => $columns,
]) ?>
</div>