<?
use yii\grid\GridView;
use app\models\User;
use yii\helpers\Url;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;



$this->title = "Список заказов";
?>
<div class="inner-section clearfix">
<?

$columns = [
        'id',
        'title',
        '_event_type.title',
        'order_date',
        'order_moderated:boolean',
        '_visible:boolean',
        'edited'
    ];
    
if(User::isThisRole(User::TYPE_ADMIN))$columns[] = '_owner_profile:raw';

$columns[] = '_orderlist_buttons:raw';


if(User::isThisRole(User::TYPE_CLIENT)) { ?>
<a href="<? echo Url::toRoute(['orders/view', 'id'=>'new'])?>">Добавить</a>
<? } ?>

<? $form = ActiveForm::Begin(); ?>
<div class="row">
	<div class="col-sm-4">
		<? echo $form->field($searchModel, 'filter_moderated')->widget(CheckboxX::classname(), []); ?>
		<? echo $form->field($searchModel, 'filter_date')->widget(Select2::classname(), [
			'data'=>['0'=>'Все', '-1'=>'Прошедшие', '1'=>"Будущие"]
		]); ?>
		<button class="btn btn-default">Фильтровать</button>
	</div>
</div>
<? ActiveForm::End(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'tableOptions' => [
	    'class' => 'table table-condensed'
    ]
]) ?>
</div>