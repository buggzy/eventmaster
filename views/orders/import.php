<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="inner-section clearfix">
	<? $form = ActiveForm::Begin(); ?>
	<? echo $form->field($model, 'text')->textArea(); ?>
	<?= Html::submitButton('Отправить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>
	<? ActiveForm::end(); ?>
</div>