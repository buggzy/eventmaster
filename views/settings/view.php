<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<? echo $form->field($model, 'settings_key')->textInput(); ?>
<? echo $form->field($model, 'labels')->textInput(); ?>
<? echo $form->field($model, 'settings_val')->textInput(); ?>

<?= Html::submitButton('Сохранить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>

<? ActiveForm::end(); ?>
</div>