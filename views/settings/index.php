<?
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
	<a href="<? echo Url::toRoute(['settings/view', 'id'=>'new'])?>">Добавить</a>
	<?= GridView::widget([
	    'dataProvider' => $model->_provider,
	    'columns' => [
			'settings_key',
			'labels',
			'settings_val',
	        [
	        	'options' => ['width'=>'1%'],
	            'content' => function ($model){
	            	return '<a href="'.Url::toRoute(['settings/view', 'id'=>$model->settings_key]).'">Редактировать</a>';
	            }
			]
	        // ...
	    ],
	]) ?>
</div>