<?
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
	<a href="<? echo Url::toRoute(['info/view', 'id'=>'new'])?>">Добавить</a>
	<?= GridView::widget([
	    'dataProvider' => $model->dataProvider,
	    'columns' => [
	        [
	        	'attribute' => 'id' ,
	        	'options' => ['width'=>'1%'],
	        	],
	        'title',
	        'url',
	        [
	        	'options' => ['width'=>'1%'],
	            'content' => function ($model){
	            	return '<a href="'.Url::toRoute(['info/view', 'id'=>$model->id]).'">Редактировать</a>';
	            }
			]
	        // ...
	    ],
	]) ?>
</div>