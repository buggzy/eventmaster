<?

$this->title = $page->title;

$content = $page->content;

$home = Yii::$app->request->hostInfo;
$content = preg_replace('/#SOCIAL_SHARE#/', '<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script><script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script><div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki" data-url="'.$home.'"></div>', $content);

$content = preg_replace('/#VK_NEWS#/', '<div id="vk_groups"></div>', $content);
$content = preg_replace('/#VK_COMMENTS#/', '<div id="vk_comments"></div>', $content);

?>

<header>
	<div class="inner">
		<h1><? echo $page->title?></h1>
	</div>
</header>
<div class="inner white-section info-page">
	<? echo $content; ?>
</div>
