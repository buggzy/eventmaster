<?
use yii\helpers\Url;
$this->title = "Личный кабинет заказчика";
?>
<div class="inner-section cabinet clearfix">
	<h2><? echo $this->title?></h2>
	<div class="row">
		<div class="col-sm-7">
			<? if(!$user->profileCompleted()) { ?>
			<p>Пожалуйста, не забудьте заполнить данные <a href="<? echo Url::toRoute(['user/view', 'id'=>$user->id])?>">профиля заказчика</a></p>
			<? } ?>
			
			<h3>Ваши заказы</h3>
			<p>
				<a href="<? echo Url::toRoute(['orders/view', 'id'=>'new']); ?>" class="btn btn-default"><i class="fa fa-plus"></i> Добавить заказ</a>
				<a href="<? echo Url::toRoute(['orders/index']); ?>" class="btn btn-default"><i class="fa fa-glass"></i>Посмотреть мои заказы</a>
			</p>
			<p>
				Добавьте заказ с описанием мероприятия, чтобы получать предложения.
				<? if(count($orders)) { ?> Вы уже разместили <? echo count($orders) ?> заказов. <? } ?>
			</p>
			
			<h3>Переговоры</h3>
			<p>
				<a href="<? echo Url::toRoute(['message/index']) ?>" class="btn btn-default"><i class="fa fa-send"></i> Посмотреть переписку</a>
			</p>
			<p>
				Вы ведете общение с <? echo $messages->count ?> потенциальными организаторами.
			</p>
		
			<? if(count($favs)) { ?>
			<h3>Избранные организаторы</h3>
			<p>
				<a href="<? echo Url::toRoute(['userlist/favs']) ?>" class="btn btn-default"><i class="fa fa-bookmark"></i> Посмотреть список</a>
			</p>
			<p>
				Вы отметили <? echo count($favs) ?> организаторов как избранных.
			</p>
			<? } ?>
			<? echo $this->render('referal'); ?>
		</div>
		<div class="col-sm-5 hidden-xs text-center">
			<? echo $this->render('@app/views/site/greetings') ?>
		</div>
	</div>
</div>