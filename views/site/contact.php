<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Задайте свой вопрос';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inner-section clearfix">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Спасибо за сообщение, ответим Вам как можно скорее!
        </div>

    <?php else: ?>
        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
        <p>
            Обратная связь. Напишите Ваш вопрос, и мы ответим Вам!
        </p>

        <div class="row">
            <div class="col-sm-4">
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
			</div>
            <div class="col-sm-4">
                    <?= $form->field($model, 'email') ?>
			</div>
            <div class="col-sm-4">
                    <?= $form->field($model, 'subject') ?>
			</div>

        </div>
        <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    <?php endif; ?>
</div>
