<footer class="pg-footer pg-section">
	<div class="inner clearfix">
		<div class="pg-footer-left-30">
			<div class="logo-wrap logo-wrap-small">
				<a href="/" class="logo">Eventblaster</a>
				<p>Не тратьте время на поиск организатора, он сам Вас найдёт!</p>
				<center>
					<a href="/news" class="btn" style="font-size: 0.857em;">Следите за новостями</a>
					<!--<h1 style="margin:3% 0 0;font-size: 30px;">8 (800) 555-3535</h1>
					<h4 style="margin:0;">Нужна помощь? Мы поможем!</h4>-->
				</center>
			</div>
		</div>
		<div class="pg-footer-right-30">
			<nav class="footer-nav">
				<ul>
					<center><li><a href="/site/contact" class="btn">Ваши пожелания и отзывы</a></li>
					<li><a href="/support" class="btn">Служба поддержки</a></li>
					<li><a href="/contacts" class="btn">Контакты</a></li></center>
				</ul>
			</nav>
		</div>
		<div class="pg-footer-right-40">
			<h4>Мы в социальных сетях:</h4>
			<ul class="stat-list" style="margin-top:0">
				<li><a href="https://www.facebook.com/Eventblaster-%D0%92%D1%8B%D0%B1%D0%B5%D1%80%D0%B8-%D0%BB%D1%83%D1%87%D1%88%D0%B5%D0%B3%D0%BE-%D0%BE%D1%80%D0%B3%D0%B0%D0%BD%D0%B8%D0%B7%D0%B0%D1%82%D0%BE%D1%80%D0%B0-449921525205720/info/?tab=overview" target="_blanc"><img src="/img/facebook_icon_white.png" style="width:45px;"></a></li>
				<li><a href="https://www.instagram.com/eventblaster.ru/" target="_blanc"><img src="/img/insta_icon_white.png" style="width:45px;"></a></li>
				<li><a href="https://vk.com/blaster_event" target="_blanc"><img src="/img/vk_icon_white.png" style="width:45px;"></a></li>
				<li><a href="https://www.youtube.com/watch?v=O3Zddla2vT4" target="_blanc"><img src="/img/youtube_icon_white.png" style="width:45px;"></a></li>
			</ul>
			<p><small>© 2015-2016 Все права защищены.</small></p>
			<p><a href="/partner">Партнерская программа</a></p>
		</div>
	</div>
</footer>
<!— Yandex.Metrika counter —> 
<script type="text/javascript"> 
(function (d, w, c) { 
(w[c] = w[c] || []).push(function() { 
try { 
w.yaCounter34840430 = new Ya.Metrika({ 
id:34840430, 
clickmap:true, 
trackLinks:true, 
accurateTrackBounce:true, 
webvisor:true 
}); 
} catch(e) { } 
}); 

var n = d.getElementsByTagName("script")[0], 
s = d.createElement("script"), 
f = function () { n.parentNode.insertBefore(s, n); }; 
s.type = "text/javascript"; 
s.async = true; 
s.src = "https://mc.yandex.ru/metrika/watch.js"; 

if (w.opera == "[object Opera]") { 
d.addEventListener("DOMContentLoaded", f, false); 
} else { f(); } 
})(document, window, "yandex_metrika_callbacks"); 
</script> 
<noscript><div><img src="https://mc.yandex.ru/watch/34840430" style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
<!— /Yandex.Metrika counter —>
