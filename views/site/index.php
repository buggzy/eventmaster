<?php

/* @var $this yii\web\View */

$this->title = 'Eventblaster - организуйте мероприятие в один клик!';
?>
	<header class="home-header-wrap pg-section">
		<div class="inner">
			<div class="logo-wrap"><a href="/" class="logo">Eventblaster</a></div>
			<p>Не тратьте время на поиск организатора, он сам Вас найдёт!</p>
		</div>
	</header>
	<section class="intro-wrap pg-section">
		<div class="inner clearfix">
			<div class="intro-item intro-left clearfix">
				<div class="intro-img"><img src="/img/p3.png"></div>
				<div class="white-section">
					<div class="arrow-left"></div>
					<p>Мне нужно организовать<br>мероприятие!</p>
					<p><a onclick="showModal(1, &quot;#modal_form&quot;)" class="btn" style="cursor:pointer;">Заказать услуги</a></p>
				</div>
			</div>
			<div class="intro-item intro-right clearfix">
				<div class="intro-img"><img src="img/p4.png"></div>
				<div class="white-section">
					<div class="arrow-right"></div>
					<p>Я могу организовать<br>мероприятие!</p>
					<p><a onclick="showModal(2, &quot;#modal_form&quot;)" class="btn" style="cursor:pointer;">Предложить услуги</a></p>
				</div>
			</div>
		</div>
	</section>
	<section class="how-we-work pg-section">
		<div class="inner">
			<h2>Как работает сервис?</h2>
			<ul class="how-we-work-list">
				<li>
					<span>1</span>
					<div class="hww-img"><img src="/img/i3.png"></div>
					<p>Размещаете заказ<br>на мероприятие</p>
				</li>
				<li>
					<span>2</span>
					<div class="hww-img"><img src="/img/i2.png"></div>
					<p>Получаете предложения</p>
				</li>
				<li>
					<span>3</span>
					<div class="hww-img"><img src="/img/i1.png"></div>
					<p>Выбираете лучшее</p>
				</li>
			</ul>
			<footer>
				<h2>Как это выглядит?</h2>
				<iframe class="video-cl" src="https://www.youtube.com/embed/O3Zddla2vT4?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen=""></iframe>
			</footer>
		</div>
	</section>
	<section id="proj-count" class="intro-wrap pg-section">
		<h2>Немного о нас</h2>
		<div class="inner clearfix">
			<div class="intro-item intro-left clearfix">
				<div class="intro-img"><img src="/img/p3.png"></div>
				<div class="white-section">
					<div class="arrow-left"></div>
					<div class="home-count"><?=$orders+25 ?></div>
					<p class="mt-none">Размещенных заказов</p>
					<p><a onclick="showModal(1, &quot;#modal_form&quot;)" class="btn" style="cursor:pointer;">Хочу мероприятие!</a></p>
				</div>
			</div>
			<div class="intro-item intro-right clearfix">
				<div class="intro-img"><img src="/img/p4.png"></div>
				<div class="white-section">
					<div class="arrow-right"></div>
					<div class="home-count"><?=$servitors+36?></div>
					<p class="mt-none">Организаторов мероприятий</p>
					<p><a onclick="showModal(2, &quot;#modal_form&quot;)" class="btn" style="cursor:pointer;">Организую мероприятие!</a></p>
				</div>
			</div>
		</div>
	</section>
<? echo $this->renderFile('@app/views/site/modal_forms_index.php'); ?>