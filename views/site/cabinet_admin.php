<?
use yii\helpers\Url;
$this->title = "Данные о системе";
?>
<div class="inner-section clearfix">
	<h2>Данные о системе</h2>
	<h3>Модерация</h3>
	<? if(count($moderation->items) == 0) { ?>
	<p>Нет задач по модерации</p>
	<? } else foreach($moderation->items as $m) { ?>
	<p><? echo $m['title']?> <? echo $m['count']; ?> <a href="<? echo $m['link']?>">Просмотреть</a></p>
	<? } ?>
	<h3>Статистика</h3>
	<? if(count($statistics->items) == 0) { ?>
	<p>Нет данных статистики</p>
	<? } else foreach($statistics->items as $m) { ?>
	<p><? echo $m['title']?> <? echo $m['count']; ?></p>
	<? } ?>

</div>