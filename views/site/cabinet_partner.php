<?
use yii\helpers\Url;
$this->title = "Личный кабинет партнера";
?>
<div class="inner-section cabinet clearfix">
	<h2><? echo $this->title?></h2>
	<div class="row">
		<div class="col-sm-7">
			<? echo $this->render('referal'); ?>
			<img src="/img/partners.png" alt="" class="img-responsive">
		</div>
		<div class="col-sm-5 hidden-xs text-center">
			<? echo $this->render('@app/views/site/greetings') ?>
		</div>
	</div>
</div>