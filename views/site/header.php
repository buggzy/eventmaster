<?
namespace app\models;

use Yii;
use yii\widgets\Menu;
	
$user = Yii::$app->user->getIsGuest() ? false : User::findIdentity(Yii::$app->user->id);
$fio = ($user && $user->fio) ? '(' . $user->fio . ')' : '';
$usertype = $user ? $user->user_type_id . ')': false;
?>
<div class="reg-enter-wrap header">
	<div class="inner clearfix">
		<div class="logo-wrap logo-wrap-small"><a href="/" class="logo">Eventblaster</a><p>Не тратьте время на поиск организатора, он сам Вас найдёт!</p></div>
		<div class="reg-enter-list main-menu">
			<? echo Menu::widget([
			    'items' => [

			    		['label'=>'Главная', 'url'=>['site/index'], 'options'=>['class'=>'home'], 'visible'=>($user !== false) && ($usertype == User::TYPE_ADMIN)],
						// меню админа
			    		['label'=>'Пользователи', 'url'=>['userlist/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Отзывы', 'url'=>['reviews/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Заказы', 'url'=>['orders/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Типы услуг', 'url'=>['eventtype/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Теги', 'url'=>['tags/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Письма', 'url'=>['mailtemplates/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Города', 'url'=>['regions/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Страницы', 'url'=>['info/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Платежи', 'url'=>['payments/transactions'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Типы подписок', 'url'=>['payments/types'], 'visible'=>($usertype == User::TYPE_ADMIN)],
			    		['label'=>'Настройки', 'url'=>['settings/index'], 'visible'=>($usertype == User::TYPE_ADMIN)],

						// меню заказчика
			    		['label'=>'Мои заказы', 'url'=>['orders/index'], 'options'=>['class'=>'orders'], 'visible'=>($usertype == User::TYPE_CLIENT)],
			    		['label'=>'Предложения от организаторов', 'url'=>['message/deals'], 'options'=>['class'=>'filter'], 'visible'=>($usertype == User::TYPE_CLIENT)],
			    		['label'=>'Каталог организаторов', 'url'=>['userlist/index'], 'options'=>['class'=>'search'], 'visible'=>($usertype == User::TYPE_CLIENT)],
			    		['label'=>'Избранное', 'url'=>['userlist/favs'], 'options'=>['class'=>'favs'], 'visible'=>($usertype == User::TYPE_CLIENT)],

						// меню исполнителя
			    		['label'=>'Поиск заказов', 'url'=>['orders/index'], 'visible'=>($usertype == User::TYPE_SERVER)],
			    		['label'=>'Избранные заказы', 'url'=>['orders/favs'], 'options'=>['class'=>'favs'], 'visible'=>($usertype == User::TYPE_SERVER)],

						// общие пункты заказчика и исполнителя
			    		['label'=>'Мои сообщения', 'url'=>['message/index'], 'options'=>['class'=>'messages'], 'visible'=>($usertype == User::TYPE_CLIENT) ||($usertype == User::TYPE_SERVER)],
			    		['label'=>'Мой профиль', 'url'=>['user/view', 'id'=>$user ? $user->id : 0], 'visible'=>($usertype == User::TYPE_CLIENT) ||($usertype == User::TYPE_SERVER)],

						// общие пункты
			    		['label'=>'Каталог организаторов', 'url'=>['userlist/index'], 'options'=>[], 'visible'=>($user === false)],
			    		['label'=>'Список заказов', 'url'=>['orders/index'], 'options'=>[], 'visible'=>($user === false)],
			    		['label'=>'Регистрация', 'url'=>['user/register'], 'options'=>['class'=>'register'], 'visible'=>($user === false)],
			    		['label'=>'Вход', 'url'=>['user/login'], 'options'=>['class'=>'login'], 'visible'=>($user === false)],
			    		['label'=>'Выход '.$fio.'', 'url'=>['user/logout'], 'options'=>['class'=>'login'], 'visible'=>($user !== false)]
			    	]
		    	]
		    ); ?>
		    <div class="subscription pull-right">
				<? if($user && $user->_paid_now) { ?>
					Подписка оплачена до: <? echo date('d-m-Y H:i', $user->pay_until) ?>
				<? } ?>
    	
		    </div>
		</div>
	</div>
</div>
<? if(Yii::$app->session->hasFlash('header-messages')){ ?>
	<section class="help regform" id="help" style="color:black;text-align:left;box-shadow: 0 0 25px 5px; margin: 0 auto;">
		
		<? echo Yii::$app->session->getFlash('header-messages') ?>
		<p><a class="btn" onclick="jQuery('#help').slideUp(500)">Закрыть</a></p>
	</section>
<? } ?>
