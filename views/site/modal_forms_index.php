<?
use yii\helpers\Url;
use app\models\User;
?>
	<div class="modal_form" id="modal_form1">
		<section class="intro-wrap pg-section">
		<h2 style="color:#000;margin:0">Заказчик</h2>
		<div class="inner clearfix">
			<div class="intro-item intro-left clearfix">
				<div class="intro-img"><img src="/img/p3.png"></div>
				<div class="white-section" style="font-size: 15px;">
					<p><a href="<? echo Url::toRoute(['orders/anon']); ?>" class="btn">Разместить заказ</a></p>
					<p class='modal-p'>Разместите свой заказ прямо сейчас, и получите десятки предложений по организации Вашего мероприятия!</p>
				</div>
			</div>
			<div class="intro-item intro-right clearfix">
				<div class="intro-img"><img src="/img/p4.png"></div>
				<div class="white-section" style="font-size: 15px;">
					<p><a href="<? echo Url::toRoute(['user/login']); ?>" class="btn">Войти на сайт</a></p>
					<p class='modal-p'>Вы уже пользовались сервисом Eventblaster? Тогда войдите на сайт, что бы дальше получать лучшие предложения по организации Вашего мероприятия!</p>
				</div>
			</div>
		</div>
		</section>
	</div>
	<div class="overlay" id="overlay1" onClick='fadeModal(1, "#modal_form")'></div>
	<div class="modal_form" id="modal_form2">
		<section class="intro-wrap pg-section">
		<h2 style="color:#000;margin:0">Организатор</h2>
		<div class="inner clearfix">
			<div class="intro-item intro-left clearfix">
				<div class="intro-img"><img src="/img/p3.png"></div>
				<div class="white-section" style="font-size: 15px;">
					<p><a href="<? echo Url::toRoute(['user/register', 'type'=>User::TYPE_SERVER]); ?>" class="btn">Зарегистрироваться</a></p>
					<p class='modal-p'>Зарегистрируйтесь на сервисе Eventblaster, что бы иметь возможность видеть заказы по организации мероприятий и предлагать заказчикам свои услуги!</p>
				</div>
			</div>
			<div class="intro-item intro-right clearfix">
				<div class="intro-img"><img src="/img/p4.png"></div>
				<div class="white-section" style="font-size: 15px;">
					<p><a href="<? echo Url::toRoute(['user/login']); ?>" class="btn">Войти на сайт</a></p>
					<p class='modal-p'>Вы уже пользовались сервисом Eventblaster? Тогда войдите на сайт, что бы дальше предлагать свои услуги заказчикам!</p>
				</div>
			</div>
		</div>
		</section>
	</div>
	<div class="overlay" id="overlay2" onClick='fadeModal(2, "#modal_form")'></div>
	<div class="modal_form" id="modal_form3">
		<section class="intro-wrap pg-section">
		<h2 style="color:#000;margin:0">Авторизация на сайте</h2>
		<div class="inner clearfix">
			<div class="intro-item intro-left clearfix">
				<div class="intro-img"><img src="/img/p3.png"></div>
				<div class="white-section" style="font-size: 15px;">
					<p><a href="http://eventblaster.picvo.ru/lk/login/client" class="btn">Я — заказчик</a></p>
					<p class='modal-p' style="">Авторизуйтесь, чтобы разместить свой заказ на проведение мероприятия, получить предложения от организаторов и выбрать наиболее подходящего!</p>
				</div>
			</div>
			<div class="intro-item intro-right clearfix">
				<div class="intro-img"><img src="/img/p4.png"></div>
				<div class="white-section" style="font-size: 15px;">
					<p><a href="http://eventblaster.picvo.ru/lk/login/agency" class="btn">Я — организатор</a></p>
					<p class='modal-p'>Авторизуйтесь, чтобы просматривать заказы на проведение мероприятий , а также предлагать свои услуги, участвуя в рейтинге организаторов</p>
				</div>
			</div>
		</div>
		</section>
	</div>
	<div class="overlay" id="overlay3" onClick='fadeModal(3, "#modal_form")'></div>
