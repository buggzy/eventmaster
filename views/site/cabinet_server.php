<?
use yii\helpers\Url;
$this->title = "Личный кабинет организатора мероприятий";
?>
<div class="inner-section clearfix">
	<h2><? echo $this->title?></h2>
		<div class="row">
			<div class="col-sm-7">
				<? if(!$user->fio || !$user->phone) { ?>
				<p>Пожалуйста, не забудьте заполнить данные <a href="<? echo Url::toRoute(['user/view', 'id'=>$user->id])?>">профиля исполнителя</a></p>
				<? } ?>
				<h3>Заказы</h3>
				<p>Поиск заказов на организацию мероприятия</p>
				<a href="<? echo Url::toRoute(['orders/index']); ?>" class="btn btn-default">Поиск заказов</a>
				<h3>Отзывы</h3>
				<p>Оставить заявку на перенос отзывов с другого сайта.</p>
				<a href="<? echo Url::toRoute(['orders/import']); ?>" class="btn btn-default">Импорт отзывов</a>
				<h3>Оплата подписки</h3>
				<p>
					<? if($user->_paid_now) { ?>
					Ваша подписка оплачена до: <? echo date('d-m-Y H:i', $user->pay_until); ?>
					<? } else { ?>
					Ваша подписка не оплачена. <a href="<? echo Url::toRoute(['payments/subscribe']); ?>" class="btn btn-default"><i class="fa fa-dollar"></i> Оплатить подписку</a>
					<? } ?>
				</p>
				<h3>Переговоры</h3>
				<p>
					Вы ведете переговоры с заказчиками по <? echo $messages->count ?> проектам.
					<a href="<? echo Url::toRoute(['message/index']) ?>" class="btn btn-default"><i class="fa fa-send"></i> Посмотреть сообщения</a>
				</p>
				<? echo $this->render('referal'); ?>
			</div>
		<div class="col-sm-5 hidden-xs text-center">
			<? echo $this->render('@app/views/site/greetings') ?>
		</div>
	</div>
</div>