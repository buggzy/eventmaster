<?
use yii\helpers\Url;
?>
<h3>Партнерская программа</h3>
<p>Ваша реферальная ссылка для партнерской программы: <?=Yii::$app->request->hostInfo . Url::toRoute(['site/ref', 'id'=>Yii::$app->user->id])?></p>
<p>Ваши начисления по партнерской программе:
	<a class="btn btn-default" href="<?=Yii::$app->request->hostInfo . Url::toRoute(['refs/view', 'id'=>Yii::$app->user->id])?>">Показать</a>
</p>