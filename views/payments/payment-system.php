<?
use app\models\SystemSettings;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\PaymentsTypes;
?>
<div class="inner-section clearfix">
	<form>
		<h2>Подписка</h2>
		<p><? echo $payment->_payment_type->title?></p>
	</form>
	<? $yandex_wallet = SystemSettings::getVal('yandex_wallet'); ?>
	<? if($yandex_wallet) { ?>
	<form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml"> 
		<p>Стоимость: <? echo $payment->_payment_type->cost?> руб.</p>
	    <input type="hidden" name="receiver" value="<? echo $yandex_wallet ?>"> 
	    <input type="hidden" name="formcomment" value="<? echo SystemSettings::getVal('yandex_order_title')?>"> 
	    <input type="hidden" name="short-dest" value="<? echo SystemSettings::getVal('yandex_order_title')?>"> 
	    <input type="hidden" name="label" value="<? echo $payment->id?>"> 
	    <input type="hidden" name="quickpay-form" value="shop"> 
	    <input type="hidden" name="targets" value="транзакция <? echo $payment->id?>">
	    <input type="hidden" name="sum" value="<? echo $payment->_payment_type->cost?>" data-type="number"> 
	    <input type="hidden" name="need-fio" value="false"> 
	    <input type="hidden" name="need-email" value="false"> 
	    <input type="hidden" name="need-phone" value="false"> 
	    <input type="hidden" name="need-address" value="false">
	    <div class="form-group">
		    <label for="sum">Метод оплаты:</label>
		    <label><input type="radio" name="paymentType" value="PC" checked>Яндекс.Деньгами</label> 
		    <label><input type="radio" name="paymentType" value="AC">Банковской картой</label> 
		    <label><input type="radio" name="paymentType" value="MC">С мобильного</label> 
	    </div>
	    <button class="btn btn-primary">Оплатить через Яндекс</button>
	</form>
	<? } ?>
	<? if(false) { ?>
	<form method="POST" action="<? Url::toRoute(['payments/robokassa_gen'])?>">
		<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
		<input type="hidden" name="sum" value="<? echo $payment->_payment_type->cost?>" data-type="number"> 
	    <input type="hidden" name="label" value="<? echo $payment->id?>"> 
		<?= Html::submitButton('Оплатить через Robokassa', ['name'=>'save', 'class' => 'btn btn-primary']) ?>
	</form>
	<? } ?>
</div>