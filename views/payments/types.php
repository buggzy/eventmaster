<?
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
	<a href="<? echo Url::toRoute(['payments/type', 'id'=>'new'])?>">Добавить</a>
	<?= GridView::widget([
	    'dataProvider' => $model->dataProvider,
	    'columns' => [
			'id',
			'title',
			'period',
			'cost',
	        [
	        	'options' => ['width'=>'1%'],
	            'content' => function ($model){
	            	return '<a href="'.Url::toRoute(['payments/type', 'id'=>$model->id]).'">Редактировать</a>';
	            }
			]
	        // ...
	    ],
	]) ?>
</div>