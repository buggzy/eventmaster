<?
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
	<a href="<? echo Url::toRoute(['payments/transaction', 'id'=>'new'])?>">Добавить</a>
	<?= GridView::widget([
	    'dataProvider' => $model->dataProvider,
	    'columns' => [
			'id',
			'_payment_type.title',
			'owner_id',
			'status',
			'timestamp',
	        [
	        	'options' => ['width'=>'1%'],
	            'content' => function ($model){
	            	return '<a href="'.Url::toRoute(['payments/transaction', 'id'=>$model->id]).'">Редактировать</a>';
	            }
			]
	        // ...
	    ],
	]) ?>
</div>