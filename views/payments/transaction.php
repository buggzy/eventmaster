<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<? echo $form->field($model, 'payment_type_id')->textInput(); ?>
<? echo $form->field($model, 'owner_id')->textInput(); ?>
<? echo $form->field($model, 'status')->textInput(); ?>


<?= Html::submitButton('Применить', ['name'=>'apply', 'class' => 'btn btn-primary']) ?>&nbsp;
<?= Html::submitButton('Сохранить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>

<? ActiveForm::end(); ?>
</div>