<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use dosamigos\ckeditor\CKEditor;

?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<? echo $form->field($model, 'title')->textInput(); ?>
<? echo $form->field($model, 'period')->textInput(); ?>
<? echo $form->field($model, 'cost')->textInput(); ?>
<? echo $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic']) ?>

<?= Html::submitButton('Применить', ['name'=>'apply', 'class' => 'btn btn-primary']) ?>&nbsp;
<?= Html::submitButton('Сохранить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>

<? ActiveForm::end(); ?>
</div>