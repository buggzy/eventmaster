<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\PaymentsTypes;

$home = Yii::$app->request->hostInfo;


?>

<div class="inner">
	<div class="row">
	<? foreach(PaymentsTypes::GetList() as $type) { ?>
		<div class="col-sm-4">
			<div class="pay-bars-items">
				<? $form = ActiveForm::begin(); ?>
				<div class="title">
					<? echo $type->title ?>
				</div>
				<div class="description">
					<?
					$content = $type->description;
					
					$user = Yii::$app->user->identity;
					$sharable = !$user->last_shared || ($user->last_shared < time() - 60*60*24*30 );
					
					if($sharable)
						$content = preg_replace('/#SOCIAL_SHARE#/', '<div class="text-center"><script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script><script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script><div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki" data-url="'.$home.'"></div></div>', $content);
					else
						$content = preg_replace('/#SOCIAL_SHARE#/', '<div class="text-center">Вы уже использовали эту возможность</div>', $content);
						
					echo $content; ?>
				</div>
				<input type="hidden" name="Payments[payment_type_id]" value="<? echo $type->id?>">
				<? if($type->cost) { ?>
					<div class="cost">Цена: <? echo $type->cost ?> руб.</div>
					<div class="text-center">
						<?= Html::submitButton('Оплатить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>
					</div>
				<? } ?>
				<? ActiveForm::end(); ?>
			</div>
		</div>
		<? } ?>
	</div>
	
</div>