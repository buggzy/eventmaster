<?
use app\models\Messages;
use app\models\User;
use yii\helpers\Url;
use yii\helpers\BaseHtml;

$need_feedback = $model->_order->_project_past && !$model->_order->servitor_id;

// @model Message: order_id, recipient - элемент, пара проект\владелец
?>
<tr>
	<td width="50%">
		<div class="project">
			<div class="title">
				Проект: <a href="<? echo Url::toRoute(['orders/view', 'id'=>$model->order_id]); ?>">
					<? echo $model->_order->title ? $model->_order->title : 'Название не указано' ?></a>
			</div>
			<div class="date">
				<? echo $model->_order->order_date ?>
			</div>
			<? if($need_feedback) { ?>
			<div class="need-feedback">
				Мероприятие состоялось. Пожалуйста, оставьте отзыв.
			</div>
			<? } ?>
			<? if($model->_order->servitor_id) { ?>
			<div class="has-feedback">
				Отзыв:
				<? echo BaseHtml::encode($model->_order->feedback_text); ?>
			</div>
			<? } ?>
		</div>
	</td>
	<td width="50%">
		<table class="noborders sm-blocks">
			<? foreach(Messages::GetDealProfiles(Yii::$app->user->id, $model->order_id) as $profile) {
				echo '<tr>';
				echo '<td class="avatar hidden-sm">';
					echo $profile->_owner->avatar ? Yii::$app->imageCache->thumb(Yii::getAlias('@web/uploads/'.$profile->_owner->avatar)) : '';
				echo '</td>';
				echo '<td>';
				echo '<a href="'.Url::toRoute(['user/view', 'id'=>$profile->_owner->id]).'">';
					echo $profile->_owner->fio ? $profile->_owner->fio : 'Имя не указано';

				echo '</a>';
					if($model->_order->servitor_id == $profile->_owner->id)echo '<div class="servitor">оставлен отзыв</div>';
				echo '</td>';
				echo '<td width="1%" class="sm-ib"><a class="btn btn-default" href="'.Url::toRoute(['message/user', 'id'=>$profile->_owner->id, 'order'=>$model->order_id]).'">Общение с организатором</a></td>';
				if($need_feedback)echo '<td class="sm-ib" width="1%"><a class="btn btn-default" href="'.Url::toRoute(['orders/feedback', 'user'=>$profile->_owner->id, 'order'=>$model->order_id]).'">Написать отзыв</a></td>';
				echo '</tr>';
			} ?>
		</table>
	</td>
</tr>
