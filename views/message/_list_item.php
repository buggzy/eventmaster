<?
use yii\helpers\BaseHtml;
?>

<div class="message <? echo $model->_direction ? "incoming" : "outgoing"; ?>">
	<div class="message-inner">
		<div class="pipka"></div>
		<div class="timestamp"><? echo date('d-m-Y H:i:s', $model->timestamp); ?></div>
		<? echo BaseHtml::encode($model->text); ?>
	</div>
</div>
