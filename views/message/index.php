<?
use yii\widgets\ListView;
use yii\helpers\Url;

$this->title = 'Диалоги';


?>
<div class="inner-section clearfix chats-list">
	<? if($provider->totalCount == 0) { ?>
			<div class="row">
				<div class="col-sm-7">
					<h2><? echo $this->title?></h2>
						Пока у Вас нет диалогов с пользователями сайта.
					</p>
				</div>
				<div class="col-sm-5 hidden-xs text-center">
					<? echo $this->render('@app/views/site/greetings') ?>
				</div>
			</div>
	<? } else { ?>
	<table>
		<? echo ListView::widget([
		    'dataProvider' => $provider,
		    'options' => [
		        'tag' => 'div',
		        'class' => 'list-wrapper',
		        'id' => 'list-wrapper',
		    ],
		    'itemView' => '_chats_item'
		]) ?>
	</table>
	<? } ?>
</div>