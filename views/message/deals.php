<?
use yii\widgets\ListView;
use yii\helpers\Url;

$this->title = 'Отклики';

?>
<div class="inner-section clearfix deals">
		<table class="sm-blocks">
			<th>Проект</th>
			<th>Отклики от</th>

		<? echo ListView::widget([
		    'dataProvider' => $provider,
		    'options' => [
		        'tag' => 'div',
		        'class' => 'list-wrapper',
		        'id' => 'list-wrapper',
		    ],
		    'itemView' => '_deals_item'
		]) ?>
		</table>
</div>