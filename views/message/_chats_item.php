<?
use app\models\Messages;
use app\models\User;
use yii\helpers\Url;
use yii\helpers\BaseHtml;

$user = User::findIdentity($model->recipient);

// @model Message: order_id, recipient - элемент, пара проект\владелец
?>

<tr>
	<td style="width: 1%;" class="hidden-xs">
		<div class="contragent">
			<div class="img-wrap">
				<? echo $user->avatar ? Yii::$app->imageCache->thumb(Yii::getAlias('@web/uploads/'.$user->avatar)) : '<img src="/img/no-photo.png">' ?>
			</div>
		</div>
	</td>
	<td>
		<div class="project">
		<? if($model->_order) { ?>
			Заказ: <? echo BaseHtml::encode($model->_order->title) ?>
		<? } else {?>
			Общение без размещенного заказа
		<? } ?>
		</div>
		<div class="user">
			<a href="<? echo Url::toRoute(['user/view', 'id'=>$model->recipient])?>" target="_blank">
				<? echo $user->fio ? BaseHtml::encode($user->fio) : 'Профиль не заполнен' ?></a>
			<? if(time() - $user->last_online < 60 ) { ?>
				<span class="online-status online">online</span>
			<? } else { ?>
				<span class="online-status offline">offline</span>
			<? } ?>
		</div>
		<div class="last">
			Последнее сообщение: <? echo date('d-m-Y H:i', $model->GetMessagesLast()->timestamp); ?>
		</div>
	</td>
	<td style="width: 1%;">
		<div class="span nowrap nounderline">
			<a href="<? echo Url::toRoute(['message/user', 'id'=>$model->recipient, 'order'=>$model->order_id])?>"><i class="fa fa-send"></i>
			<? $unread = $model->GetMessagesUnread()->count(); ?>
			<? if($unread){ echo "($unread новых)"; } else { ?>
			читать
			<? } ?>
			</a>
		</div>
	</td>
</tr>

