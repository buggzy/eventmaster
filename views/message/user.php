<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use newerton\fancybox\FancyBox;

$this->title = 'Переписка';

$order = $model->_order;

?>

<? echo FancyBox::widget([
    'target' => 'a[data-fancybox-group]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]); ?>


<div class="inner-section chat-section clearfix">
	<div class="chat-with-user">
		<? $form = ActiveForm::begin() ?>
		<div class="chat-hdr">
			<div class="chat-title">
					Переписка с пользователем <a target="_blank" href="<? echo Url::toRoute(['user/view', 'id'=>$recipient->id])?>">
						<? echo $recipient->fio ? $recipient->fio : 'не заполнен профиль' ?></a>
					<? if($model->_order) { ?>
					по заказу <b><? echo $model->_order->title ?></b>
					<? } ?>
				</div>
				<? if($order && User::isThisRole(User::TYPE_SERVER)) { ?>
				<div class="contacts">
					<? if($model->getMessages()->count) { ?>
					Контакты заказчика:
					<ul>
						<? if($order->phone && $order->phone_show) { ?><li class="phone"><? echo $order->phone ?></li><? } else { ?><li class="phone">Телефон скрыт по желанию Заказчика</li><? } ?>
						<? if($order->fio) { ?><li class="fio"><? echo $order->fio ?></li><? } ?>
						<? if($order->email) { ?><li class="email"><? echo $order->email ?></li><? } ?>
						<? if($order->social) { ?><li class="social"><? echo $order->social ?></li><? } ?>
					</ul>
					<? } else { ?>
					<? $contacts = ['имя'];
					if($order->phone && $order->phone_show)
						$contacts[] = 'телефон';
					if($order->email)
						$contacts[] = 'e-mail';
						
					$contacts = join(', ', $contacts);
					?>
					Контакты заказчика (<?=$contacts?>) будут доступны, когда Вы напишете ему первое сообщение
					<? } ?>
				</div>
				<? } ?>
		</div>
		<? echo ListView::widget([
		    'dataProvider' => $model->getMessages(),
		    'options' => [
		        'tag' => 'div',
		        'class' => 'list-wrapper',
		        'id' => 'list-wrapper',
		    ],
		    'itemView' => '_list_item'
		]) ?>
		<? echo $form->field($model, 'text')->textArea() ?>
		<div class="buttons">
	        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
		</div>
        <? if(!User::isThisRole(User::TYPE_SERVER) && $recipient->_portfolio_count) { ?>
  		<div class="portfolio-wrap mt">
			<a href="#portfolio-unhide-<?=$model->id?>" class="portfolio-toggle">
				<span class="show-on-inactive">Показать портфолио исполнителя</span>
				<span class="show-on-active">Скрыть портфолио исполнителя</span>
			</a>
			<div class="portfolio" id="portfolio-unhide-<?=$model->id?>" data-url="<? echo Url::toRoute(['portfolio/index', 'id'=>$recipient->id])?>"></div>
		</div>
		<? } ?>
		<? ActiveForm::end(); ?>


	</div>
</div>