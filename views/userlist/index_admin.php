<?
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\i18n\Formatter;
use app\models\User;

$this->title = 'Список пользователей';

?>
<div class="inner-section clearfix">
<?= GridView::widget([
    'dataProvider' => $model->dataProvider,
    'formatter' => [
    	'class' => Formatter::class,
    	'booleanFormat'=>['Нет', 'Да']
    ],
    'columns' => [
        'id',
        'fio',
        'phone',
        [
        	'attribute' => 'user_type.title',
        ],
        'email',
        'moderated:boolean',
        [
        	'content' => function($model){
        		if($model->user_type_id!=User::TYPE_CLIENT)
        			return '';
        			
        		$retval = '';
        			
        		foreach($model->_orders as $order){
        			$retval .= '<div class="item"><a href="'.Url::toRoute(['orders/view', 'id'=>$order->id]).'">' . $order->title . '</a></div> ';
        		}
        		
        		return $retval;
        	},
        	'label' => 'Заказы'
    	],
        [
        	'content' => function($model){
        		if($model->created_at)
	        		return date('d-m-Y h:i', $model->created_at);
	        	else
	        		return '';
        	},
        	'label' => 'Дата регистрации'
		],
        [
        	'content' => function($model){
        		if($model->ref_parent){
	        		return Html::encode(User::findIdentity($model->ref_parent)->fio) . ' ('.$model->ref_parent.')';
        		}
	        	else
	        		return '';
        	},
        	'label' => 'Реферал'
		],
        [
        	'content' => function($model){
        		if($model->pay_until)
	        		return date('d-m-Y', $model->pay_until);
	        	else
	        		return '';
        	},
        	'label' => 'Оплачено до'
		],
        [
        	'content' => function($model){
        	    	return '<a href="'.Url::toRoute(['user/view', 'id'=>$model->id] ).'">Профиль</a>';
				}
        	]
        // ...
    ],
]) ?>
</div>