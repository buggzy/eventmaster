<?
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use app\models\Regions;
use app\models\EventType;
use yii\helpers\Html;
use newerton\fancybox\FancyBox;
use kartik\select2\Select2;


$this->title = isset($title) ? $title : "Поиск организаторов мероприятий";

?>
<? echo FancyBox::widget([
    'target' => 'a[data-fancybox-group]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]); ?>

<div class="bars-userlist-section clearfix">
	<div class="inner">
		<div class="cutted-form">
		<?php $form = ActiveForm::begin(); ?>
		<h2 class="text-center"><? echo $this->title ?></h2>
		<? if($filter) { ?>
		<div class="row">
			<div class="col-lg-3 col-sm-6">
				<? echo $form->field($model, 'region')->widget(Select2::classname(), [
				    'data' => Regions::GetList(),
				    'language' => 'ru',
				    'options' => [
				    	'placeholder' => 'Выберите город',
				    ],
				    'pluginOptions' => [
						'tags' => false,
				    	'multiple' => false,
				        'allowClear' => true
				    ],
				]);
				?>
			</div>
			<div class="col-lg-3 col-sm-6">
				<? echo $form->field($model, 'sortby')->dropDownList($model->getSortTypes()); ?>
			</div>
			<div class="col-lg-3 col-sm-6">
				<? $types = EventType::GetTreeList(0); array_unshift($types, 'Любой'); ?>
				<? echo $form->field($model, 'type')->dropDownList($types, EventType::GetTreeList(0)); ?>
			</div>
			<div class="col-lg-3 col-sm-6">
				<? echo $form->field($model, 'byname')->textInput(); ?>
			</div>
		</div>
		<div class="text-center">
			<?= Html::submitButton('<i class="fa fa-search"></i> Искать', ['name'=>'apply', 'class' => 'btn btn-primary wide-btn']) ?>
		</div>
		<? } ?>
		<?php ActiveForm::end(); ?>
		</div>
	
		<? if($model->dataProvider->totalCount == 0) { ?>	
		<div class="inner-section clearfix">
			<div class="row">
				<div class="col-sm-7">
					<h2><? echo isset($notfound) ? $notfound : 'Ничего не найдено'?></h2>
				</div>
				<div class="col-sm-5 hidden-xs text-center">
					<? echo $this->render('@app/views/site/greetings') ?>
				</div>
			</div>
		</div>
		<? } else { ?>
		
		<?= ListView::widget([
		    'dataProvider' => $model->dataProvider,
		    'options' => [
		        'tag' => 'div',
		        'class' => 'list-wrapper',
		        'id' => 'list-wrapper',
		    ],
		    'itemView' => '_list_item'
		]) ?>
		
		<? } ?>

	</div>
</div>
