<?
use yii\helpers\Url;
use yii\helpers\BaseHtml;
	
?>

<div class="userlist-item">
	<div class="title">
		<i class="fa fa-user"></i> <a href="<? echo Url::toRoute(['user/view', 'id'=>$model->id])?>">
			<? echo $model->fio ? BaseHtml::encode($model->fio) : 'Имя не указано' ?>
		</a> (<? echo $model->_region_title?>)
		<? if(time() - $model->last_online < 60 ) { ?>
			<span class="online-status online">online</span>
		<? } else { ?>
			<span class="online-status offline">offline</span>
		<? } ?>
		<? $reviews = $model->_reviews_count; ?>
		<span class="reviews">
			<? unset($reviews['total']); foreach($reviews as $key=>$v){ ?>
				<a href="<? echo Url::toRoute(['user/reviews', 'id'=>$model->id, 'type'=>$key])?>" class="<?=$key?>"><?=$reviews[$key]?></a>
			<? } ?>
		</span>
		<div class="servitor-type">
			<? echo $model->_event_type_title ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2 col-sm-3 mb">
			<div class="photo">
				<? echo $model->avatar ? Yii::$app->imageCache->thumb(Yii::getAlias('@web/uploads/'.$model->avatar)) : '<img class="nophoto" src="/img/p4.png">' ?>
			</div>
		</div>
		<div class="col-sm-9">
			<? if($model->taglist_ids) { ?>
			<div class="services">
				<div class="taglist">
					Услуги:
					<ul>
					<? foreach($model->_taglist_text as $i)
						echo '<li><i class="fa fa-tag"></i> ' . BaseHtml::encode($i->title) . "</li> "
					?>
					</ul>
				</div>
			</div>
			<? } ?>
			<? if($model->comments) { ?>
			<div class="resume">
				<hr>
				<div class="about-text">
					<? echo BaseHtml::encode($model->comments) ?>
				</div>
				<hr>
			</div>
			<? } ?>
			<? if($model->_portfolio_count) { ?>
			<div class="portfolio-wrap mb">
				<a href="#portfolio-unhide-<?=$model->id?>" class="portfolio-toggle">
					<span class="show-on-inactive">Показать портфолио</span>
					<span class="show-on-active">Скрыть портфолио</span>
				</a>
				<div class="portfolio" id="portfolio-unhide-<?=$model->id?>" data-url="<? echo Url::toRoute(['portfolio/index', 'id'=>$model->id])?>"></div>
			</div>
			<? } ?>
			<div class="buttons">
				<? if(!Yii::$app->user->getIsGuest()) { ?>
				<?
					$in_favs = !Yii::$app->user->getIsGuest() && in_array($model->id, Yii::$app->user->identity->_fav_ids) ? 'in-favs' : '';
				?>
				<a href="<? echo Url::toRoute(['user/fav_toggle', 'id'=>$model->id ])?>" class="btn btn-primary ajax-link favs-toggle <?=$in_favs?>">
					<span class="off"><i class="fa fa-bookmark"></i> В избранное</span>
					<span class="on"><i class="fa fa-close"></i> Убрать из избранного</span>
				</a>
				<? } ?>
				<a class="btn btn-primary" target="_blank" href="<? echo Url::toRoute(['user/view', 'id'=>$model->id, 'newwin'=>1])?>"><i class="fa fa-info"></i> Подробнее об организаторе</a>
				<a class="btn btn-primary" href="<? echo Url::toRoute(['message/user', 'id'=>$model->id])?>"><i class="fa fa-send"></i> Связаться</a>
			</div>

		</div>
	</div>
</div>