<?
use app\models\Tags;
use app\models\User;
use app\models\Regions;
use kartik\select2\Select2;
use yii\helpers\Url;
use newerton\fancybox\FancyBox;
use yii\helpers\Html;

?>
<br>
		<table>
			<tr>
			<? if($model->taglist_ids) : ?>
			<td>
			<div class="services">
				<div class="taglist">
					<label>Услуги:</label>
						<ul>
						<? foreach($model->_taglist_text as $i)
							echo '<li><i class="fa fa-tag"></i> ' . Html::encode($i->title) . "</li> "
						?>
						</ul>
					</div>
				</div>
			</td>
			<? endif ?>
			<td>
				<label>Город(а): </label>
				<? echo $model->_region_title; ?>
			</td>
			<td>
				<label>Тип организатора:</label>
				<? echo $model->_event_type_title; ?>
			</td>
			</tr>
		</table>


<? if(User::isThisRole(User::TYPE_ADMIN)) echo $form->field($model, '_pay_until')->textInput(['readonly'=>'true']); ?>
<? if($model->_portfolio_count) { ?>
<label>Портфолио</label>
<div class="portfolio" data-ajax-embed="<? echo Url::toRoute(['portfolio/index', 'id'=>$model->id])?>"></div>
<? echo FancyBox::widget([
    'target' => 'a[data-fancybox-group]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]); ?>
<? } ?>
