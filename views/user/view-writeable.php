<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\UserTypes;
use app\models\User;
use app\models\EventType;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\date\DatePicker;

$this->title = 'Редактирование профиля';

?>
<div class="inner-section clearfix hilite-required">
<? $form = ActiveForm::Begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
<h2><? echo $this->title?>
		<? if($model->user_type_id == USER::TYPE_SERVER) { ?>
		<? $reviews = $model->_reviews_count; ?>
		<span class="reviews">
			<? unset($reviews['total']); foreach($reviews as $key=>$v){ ?>
				<a href="<? echo Url::toRoute(['user/reviews', 'id'=>$model->id, 'type'=>$key])?>" class="<?=$key?>"><?=$reviews[$key]?></a>
			<? } ?>
		</span>
		<? } ?>
</h2>
<div class="row">
	<div class="col-sm-3">
		<? echo $form->field($model, 'avatar')->widget(FileInput::classname(), [
			    'options' => ['accept' => 'image/*'],
		        'pluginOptions' => [
		        'showPreview' => true,
		        'showCaption' => true,
		        'showRemove' => false,
		        'showUpload' => false,
		        'initialPreview'=>
			        $model->avatar ? [ Yii::$app->imageCache->thumb(Yii::getAlias('@web/uploads/'.$model->avatar)) ] : [],
			        'uploadExtraData' => [
			        	'owner'=>$model->id
			        ],
			        'maxFileCount' => 1
			    ]
		]); ?>
	</div>
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-4">
				<? echo $form->field($model, 'fio')->textInput(); ?>
			</div>
			<div class="col-sm-4">
				<? echo $form->field($model, 'email')->textInput(); ?>
			</div>
			<div class="col-sm-4">
				<? echo $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
				    'mask' => '+7 (999) 999-99-99',
				]) ?>
			</div>
		</div>
		<? echo $form->field($model, 'comments')->textArea(); ?>
	</div>
</div>
<? if(User::isThisRole(User::TYPE_ADMIN)) { ?>
<div class="row">
	<div class="col-sm-3">
		<? echo $form->field($model, 'id')->textInput(['readonly' => true]); ?>
	</div>
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-6">
				<? echo $form->field($model, 'username')->textInput(['readonly' => true]); ?>
			</div>
			<div class="col-sm-6">
				<? echo $form->field($model, 'email'); ?>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<? echo $form->field($model, 'moderated')->dropDownList(
				[0=>'Нет',1=>'Да']
		); ?>
	</div>
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-6">
				<? echo $form->field($model, 'user_type_id')->dropDownList(
						UserTypes::GetList()
				); ?>
			</div>
			<div class="col-sm-6">
				<? echo $form->field($model, '_pay_until')->widget(DatePicker::classname(), [
						'language' => 'ru',
						'removeButton' => false,
					    'pluginOptions' => [
					        'autoclose'=>true,
					        'format' => 'dd-mm-yyyy'
					    ]
				]); ?>
			</div>
		</div>
	</div>
</div>

<? } ?>






<? if($model->user_type_id == User::TYPE_SERVER)echo $this->render('@app/views/user/server-writeable.php', ['model'=>$model, 'form'=>$form]); ?>


<div class="form-group">
	<?= Html::submitButton('Сохранить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>
	<a class="btn btn-default pass-unhide" href="#changepass">Изменить пароль</a>
</div>
<div class="row">
	<div class="col-sm-3">
		<div id="changepass" class="<? echo !$model->password1 && !$model->password2 ? 'hidden' : ''?>">
			<div class="form-group">
					<? echo $form->field($model, 'password1')->textInput(); ?>
			</div>
			<div class="form-group">
					<? echo $form->field($model, 'password2')->textInput(); ?>
			</div>
			<?= Html::submitButton('Задать пароль', ['name'=>'save', 'class' => 'btn btn-primary']) ?>
		</div>
	
	</div>
</div>

<? ActiveForm::end(); ?>
</div>