<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\UserTypes;
use app\models\User;
use yii\helpers\Url;

?>
<div class="inner-section clearfix user-type-choose">
<? $form = ActiveForm::Begin(); ?>

<h1>Выберите свою роль</h1>

<div class="row text-center">
	<div class="col-sm-6">
		<label for="client">
		<h2>Заказчик</h2>
		<p>Хочу разместить заказ на проведение мероприятий и выбирать лучшие предложения от организаторов мероприятий!</p>
		<img src="/img/p3.png" alt="">
		</label>
	</div>
	<div class="col-sm-6">
		<label for="servitor">
		<h2>Организатор</h2>
		<p>Хочу просматривать заказы по организации мероприятий и предлагать свои услуги заказчикам!</p>
		<img src="/img/p4.png" alt="">
		</label>
	</div>
</div>

<input type="radio" name="User[user_type_id]" value="2" id="client">
<input type="radio" name="User[user_type_id]" value="3" id="servitor">

<? ActiveForm::end(); ?>
</div>