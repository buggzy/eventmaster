<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use richweber\recaptcha\Captcha;

$this->title = 'Восстановление пароля';
?>
<div class="site-login">
    <?php $form = ActiveForm::begin(['id'=>'login-form']); ?>
	<div class="left-chuvak">
	</div>
	<div class="login-internal">
	    <h3 class="text-center">Восстановление пароля</h3>
	    <? echo $form->field($model, 'username')->textInput(['readonly'=>'readonly']); ?>
	    <? echo $form->field($model, 'password1')->textInput(); ?>
	    <? echo $form->field($model, 'password2')->textInput(); ?>
	    

		<button class="btn btn-default">Сменить пароль</button>
		<div class="clearfix"></div>

	</div>
	<div class="right-chuvak">
	</div>
	
    <?php ActiveForm::end(); ?>
</div>
