<?
use yii\bootstrap\Tabs;

$this->title = "Отзывы пользователя ".$user->fio;

?>

<div class="inner-section clearfix">
<?
$items = [
    	[
    		'label' => 'Все отзывы ('.$reviews['total']->count().')',
    		'active' => ($type == 'total'),
    		'content' => $this->render('_reviews_list', ['reviews'=>$reviews['total']])
		],
    	[
    		'label' => 'Положительные ('.$reviews['positive']->count().')',
    		'active' => ($type == 'positive'),
    		'content' => $this->render('_reviews_list', ['reviews'=>$reviews['positive']])
		],
    	[
    		'label' => 'Нейтральные ('.$reviews['neutral']->count().')',
    		'active' => ($type == 'neutral'),
    		'content' => $this->render('_reviews_list', ['reviews'=>$reviews['neutral']])
		],
    	[
    		'label' => 'Негативные ('.$reviews['negative']->count().')',
    		'active' => ($type == 'negative'),
    		'content' => $this->render('_reviews_list', ['reviews'=>$reviews['negative']])
		],
    	[
    		'label' => 'Отзывы с других сайтов ('.$reviews['imported']->count().')',
    		'active' => ($type == 'imported'),
    		'content' => $this->render('_reviews_list', ['reviews'=>$reviews['imported']])
		]
	];

echo Tabs::widget([
    'items' => $items
    ]);
?>
</div>