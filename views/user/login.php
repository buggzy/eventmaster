<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use rmrevin\yii\ulogin\ULogin;
use yii\helpers\Url;


$this->title = isset($message) ? $message : 'Вход на сайт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
	<div class="left-chuvak">
	</div>
	<div class="login-internal">
	    <h3 class="login-options">
	    	<? $titles = ['Регистрация' => Url::toRoute(['user/register']), 'Вход на сайт'  => Url::toRoute(['user/login'])];
	    	if(!in_array($this->title, $titles))
	    		$titles[$this->title] = '';
	    	foreach($titles as $title => $link) {
	    		if($title == $this->title)
	    			echo '<span>' . $title . '</span>';
	    		else
	    			echo '<a href="'.$link.'">' . $title . '</a>';
	    	}
	    	?>
	    </h3>
	
	    <?php $form = ActiveForm::begin([
	        'id' => 'login-form'
	    ]); ?>
	
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <div class="form-group">
                <?= Html::submitButton($this->title, ['class' => 'btn btn-primary login-button', 'name' => 'login-button']) ?>
				<? echo ULogin::widget([
				    // widget look'n'feel
				    'display' => ULogin::D_PANEL,
				
				    // required fields
				    'fields' => [ULogin::F_FIRST_NAME, ULogin::F_LAST_NAME, ULogin::F_EMAIL, ULogin::F_PHONE, ULogin::F_CITY, ULogin::F_COUNTRY, ULogin::F_PHOTO_BIG],
				
				    // optional fields
				    'optional' => [ULogin::F_BDATE],
				
				    // login providers
				    'providers' => [ULogin::P_VKONTAKTE, ULogin::P_FACEBOOK, ULogin::P_TWITTER, ULogin::P_GOOGLE, ULogin::P_ODNOKLASSNIKI],
				
				    // login providers that are shown when user clicks on additonal providers button
				    'hidden' => [],
				
				    // where to should ULogin redirect users after successful login
				    'redirectUri' => Url::toRoute(['user/ulogin', 'backurl'=>Yii::$app->request->getQueryParam('backurl')]),
				
				    // optional params (can be ommited)
				    // force widget language (autodetect by default)
				    'language' => ULogin::L_RU,
				
				    // providers sorting ('relevant' by default)
				    'sortProviders' => ULogin::S_RELEVANT,
				
				    // verify users' email (disabled by default)
				    'verifyEmail' => '0',
				
				    // mobile buttons style (enabled by default)
				    'mobileButtons' => '1',
				]); ?>
				<br>
				<div class="">
				<a href="<? echo Url::toRoute(['user/reset'])?>">Забыли пароль?</a>
				<br>
				<a href="<? echo Url::toRoute(['site/contact'])?>">Служба технической поддержки</a>
				</div>
        </div>
	
	</div>
	<div class="right-chuvak">
	</div>
	
    <?php ActiveForm::end(); ?>
</div>
