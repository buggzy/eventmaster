<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use richweber\recaptcha\Captcha;


$this->title = 'Восстановление пароля';
?>
<div class="site-login">
    <?php $form = ActiveForm::begin(['id'=>'login-form']); ?>
	<div class="left-chuvak">
	</div>
	<div class="login-internal text-center">
		
	    <h3>Восстановление пароля</h3>

		<? if($status === 0) { ?>
	        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
	        <?= Captcha::widget() ?>
	        <?= Html::submitButton('Восстановить пароль', ['class' => 'btn btn-primary login-button', 'name' => 'login-button']) ?>
        <? } ?>
		<? if($status === 1) { ?>
		    <p class="well">Ссылка для восстановления пароля отправлена на электронную почту</p>
        <? } ?>
		<? if($status === 2) { ?>
		    <p class="well">Не найден такой пользователь</p>
        <? } ?>
		<? if($status === 3) { ?>
		    <p class="well">Ссылка недействительна. Возможно, вы пытаетесь повторно пройти по ссылке, уже использованной ранее для восстановления пароля.</p>
        <? } ?>

	</div>
	<div class="right-chuvak">
	</div>
	
    <?php ActiveForm::end(); ?>
</div>
