<?
use yii\helpers\Url;

$rates = [
		'-2'=>'Очень плохо',
		'-1'=>'Плохо',
		'0' => 'Нормально',
		'1' => 'Хорошо',
		'2' => 'Отлично!'
		];
		
?>

<div class="reviews-list">
	<? if(!$reviews->count()) { ?>
		Нет отзывов
	<? } else { ?>
		<? foreach($reviews->all() as $review) { ?>
			<div class="review-item">
				<? if(get_class($review) != 'app\models\ImportedReviews') { ?>
				<div class="fio">
					Заказчик: <b><? echo $review->owner->fio ?></b>
				</div>
				<div class="project">
					Проект: <b><? echo $review->title ?></b>
				</div>
				<div class="rate">
					Оценка: <b><? echo $rates[$review->feedback_rate] ?></b>
				</div>
				<i class="fa fa-quote-left pull-left"></i>
				<div class="review-body">
					<div class="review-text">
						<? echo $review->feedback_text ?>
					</div>
				</div>
				<i class="fa fa-quote-right"></i>
				<? } else { ?>
				<div class="name">
					<? echo $review->name ?>
					<span class="date"><? echo date('d-m-Y', $review->created) ?></span>
				</div>
				<i class="fa fa-quote-left pull-left"></i>
				<div class="review-body">
					<div class="review-text">
						<? echo $review->text ?>
					</div>
				</div>
				<i class="fa fa-quote-right"></i>
				<? } ?>
			</div>
		<? } ?>
	<? } ?>
</div>