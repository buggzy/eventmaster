<?
use app\models\EventType;
use app\models\User;
use app\models\Regions;
use app\models\Tags;
use kartik\select2\Select2;
use yii\helpers\Url;
use newerton\fancybox\FancyBox;
use dosamigos\fileupload\FileUpload;


?>

<div class="row">
	<div class="col-sm-3">
		<? echo $form->field($model, '_region_ids')->widget(Select2::classname(), [
		    'data' => Regions::GetList(0),
		    'language' => 'ru',
		    'options' => [
		    	'placeholder' => 'Выберите город',
		    ],
		    'pluginOptions' => [
				'tags' => true,
		    	'multiple' => true,
		        'allowClear' => true
		    ],
		]);
		?>
	</div>
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-6">
				<? echo $form->field($model, 'event_type_id')->dropDownList(
					EventType::GetTreeList()
				); ?>
			</div>
			<div class="col-sm-6">
				<?
				echo $form->field($model, '_tags_multiselect')->widget(Select2::classname(), [
				    'data' => Tags::GetList(),
				    'language' => 'ru',
				    'options' => [
				    	'placeholder' => 'Выберите услуги',
				    ],
					'showToggleAll' => false,
				    'pluginOptions' => [
						'tags' => true,
				    	'multiple' => true,
				        'allowClear' => true,
				        'maximumSelectionLength' => User::MAX_TAGS
				    ],
				]);
				?>
			</div>
		</div>
	</div>
</div>
<p>Оставить заявку на перенос отзывов с другого сайта.</p>
<a href="<? echo Url::toRoute(['orders/import']); ?>" class="btn btn-default" target="_blank">Импорт отзывов</a>
<br><br>
<p>Нельзя размещать контент (фото/видео), в котором содержатся ваши контактные данные, а именно: номера телефонов и ссылки на сайт. Спасибо за понимание!!
</p>
<label>Портфолио</label>
<?
echo FancyBox::widget([
    'target' => 'a[data-fancybox-group]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]);
?>
<div class="portfolio-add-image">
	<span>Добавить фото:</span>
	<?= FileUpload::widget([
		'name' => 'ajax_portfolio_upload',
	    'url' => ['portfolio/upload', 'id' => $model->id],
	    'options' => ['accept' => 'image/*', 'multiple' => 'true'],
	    'clientOptions' => [
	        'maxFileSize' => 2000000
	    ],
	    'clientEvents' => [
	        'fileuploaddone' => 'function(e, data) {
	        	$(".portfolio").trigger("reload");
	                            }',
	        'fileuploadfail' => 'function(e, data) {
	                            }',
	    ],
	]); ?>
</div>
<div class="portfolio-add-video" data-pseudoform="<? echo Url::toRoute(['portfolio/addlink', 'id'=>$model->id]); ?>">
	<button class="btn btn-success pseudoform-submit">Добавить видео</button>
	<div class="add-video-wrap">
		<input type="text" name="add_video" placeholder="Ссылка на видео: https://www.youtube.com/watch?v= или код iframe">
	</div>
</div>
<div class="portfolio" data-ajax-embed="<? echo Url::toRoute(['portfolio/index', 'id'=>$model->id])?>"></div>
