<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\User;
use app\models\EventType;
use kartik\file\FileInput;
use yii\helpers\Url;

$this->title = $model->fio ? $model->fio : 'Просмотр профиля';

?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<h2><? echo $this->title?>
		<? if($model->user_type_id == USER::TYPE_SERVER) { ?>
		<? $reviews = $model->_reviews_count; ?>
		<span class="reviews">
			<? unset($reviews['total']); foreach($reviews as $key=>$v){ ?>
				<a href="<? echo Url::toRoute(['user/reviews', 'id'=>$model->id, 'type'=>$key])?>" class="<?=$key?>"><?=$reviews[$key]?></a>
			<? } ?>
		</span>
		<? } ?>
</h2>

<? if($model->_paid_now) { ?>
<div class="row">
	<div class="col-sm-3">
		<? echo $form->field($model, 'phone')->textInput(['readonly'=>'true']); ?>
	</div>
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-6">
				<? echo $form->field($model, 'fio')->textInput(['readonly'=>'true']); ?>
			</div>
			<div class="col-sm-6">
				<? echo $form->field($model, 'email')->textInput(['readonly'=>'true']);; ?>
			</div>
		</div>
	</div>
</div>

<? } ?>
<div class="row">
	<div class="col-sm-3">
		<label>Фото</label>
		<div class="avatar">
			<? echo $model->avatar ? Yii::$app->imageCache->thumb(Yii::getAlias('@web/uploads/'.$model->avatar)) :
			'<img height="150" width="150" src="/img/no-photo.png">' ?>
		</div>
	</div>
	<div class="col-sm-9">
		<label>О себе:</label>
		<? if($model->moderated) { ?>
		<? echo Html::encode($model->comments); ?>
		<? } else { ?>
		Текст еще не прошел модерацию, зайдите позже
		<? } ?>
	</div>
</div>



<? if($model->user_type_id == User::TYPE_SERVER)echo $this->renderFile('@app/views/user/server-readonly.php', ['model'=>$model, 'form'=>$form]); ?>
<? if(!Yii::$app->request->get('newwin')) : ?>
<a class="btn btn-primary" href="<?=Yii::$app->request->referrer;?>">Назад</a>
<? endif ?>

<? ActiveForm::end(); ?>
</div>