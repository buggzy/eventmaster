<?
use yii\grid\GridView;
use app\models\User;
use yii\helpers\Url;

$this->title = 'Перенос отзывов с других сайтов';

?>
<div class="inner-section clearfix">
	<a href="<? echo Url::toRoute(['reviews/view', 'id'=>'new'])?>">Добавить</a>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        '_servitor.fio',
        'name',
        'text',
		'_created',
        [
        	'content' => function($model){
        		return ''.
        			'<a href="'.Url::toRoute(['reviews/view', 'id'=>$model->id]).'">Редактировать</a> '.
        			'<a href="'.Url::toRoute(['reviews/remove', 'id'=>$model->id]).'">Удалить</a>'.
        			'';
        	}
        ]
    ],
]) ?>
</div>