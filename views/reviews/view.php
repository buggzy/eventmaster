<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\User;
use kartik\date\DatePicker;


$this->title = 'Редактирование отзыва - Перенос отзывов с других сайтов';

?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<? echo $form->field($model, 'servitor_id')->dropDownList(
	User::GetList()
); ?>
<? echo $form->field($model, 'name')->textInput(); ?>
<? echo $form->field($model, '_created')->widget(DatePicker::classname(), [
				'language' => 'ru',
				'removeButton' => false,
			    'pluginOptions' => [
			        'autoclose'=>true,
			        'format' => 'dd-mm-yyyy'
			    ]
		]); ?>
<? echo $form->field($model, 'text')->textArea(); ?>

<?= Html::submitButton('Сохранить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>&nbsp;
<?= Html::submitButton('Назад', ['name'=>'back', 'class' => 'btn btn-primary']) ?>

<? ActiveForm::end(); ?>
</div>