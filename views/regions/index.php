<?
use yii\grid\GridView;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
	<a href="<? echo Url::toRoute(['regions/view', 'id'=>'new'])?>">Добавить</a>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'title',
        [
        	'content' => function($model){
        		$actions = '<a href="'.Url::toRoute(['regions/view', 'id'=>$model->id]).'">Редактировать</a> ';
        		$actions .= '<a href="'.Url::toRoute(['regions/remove', 'id'=>$model->id]).'">Удалить</a> ';
        		if(!$model->moderated)
	        		$actions .= '<a href="'.Url::toRoute(['regions/approve', 'id'=>$model->id]).'">Утвердить</a> ';
	        		
        		return $actions;
        		
        	}
    	]
    ],
]) ?>
</div>