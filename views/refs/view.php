<?
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Начисления по партнерской программе';

?>
<div class="inner-section clearfix">
	
	<h2><?=$this->title?></h2>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        '_for.fio',
        'amount',
        'timestamp:date'
    ],
]) ?>
</div>