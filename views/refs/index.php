<?
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Пользователи с рефералами';

?>
<div class="inner-section clearfix">
	
	<h2><?=$this->title?></h2>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'fio',
        [
        	'label' => 'Рефералы',
        	'content' => function ($model){
        		$retval = [];
        		foreach($model->_ref_list as $u){
        			$retval[] = '<a href="'.Url::toRoute(['user/view', 'id'=>$u->id]).'">' . ($u->fio ? $u->fio : '#'.$u->id) . '</a>';
        		}
        		return join(', ', $retval);
        	}
        ]
    ],
]) ?>
</div>