<?
use yii\helpers\Url;
use app\models\Portfolio;

?>


<div class="ajax-portfolio">
<? if(count($model) == 0) { ?>
В портфолио пока нет работ
<? } else {
	?>
	<div class="portfolio-content" id="unhide-portfolio-<?=$id?>">
	<? if(!empty($model->_images)) { ?>
	<div class="images">
		<h4>Фотографии</h4>
	<? foreach($model->_images as $mod) { ?>
		<div class="portfolio-item image-item">
			<div class="img">
				<a data-fancybox-group="portfolio" href="/uploads/<? echo $mod->path ?>"><? echo Yii::$app->imageCache->thumb(Yii::getAlias('@web/uploads/'.$mod->path)); ?></a>
			</div>
		<? buttons($writeable, $moderation, $mod); ?>
		</div>
	<? } ?>
	</div>
	<? } ?>
	<? if(!empty($model->_youtube) || !empty($model->_iframe)) { ?>
	<div class="videos">
		<h4>Видео с участием организатора</h4>
	<? foreach($model->_youtube as $mod) { ?>
		<div class="portfolio-item youtube-item">
			<div class="video">
				<a data-fancybox-group="portfolio_yt_<? echo $mod->id?>" href="#ytvideo_<? echo $mod->id?>"><img src="http://i.ytimg.com/vi/<? echo $mod->path?>/mqdefault.jpg" alt=""></a>
				<div class="hidden-video" id="ytvideo_<? echo $mod->id?>">
					<iframe width="100%" height="100%" src="https://www.youtube.com/embed/<? echo $mod->path?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		<? buttons($writeable, $moderation, $mod); ?>
		</div>
	<? } ?>
	<? foreach($model->_iframe as $mod) { ?>
		<div class="portfolio-item iframe-item">
			<div class="video">
				<iframe src="<? echo $mod->path ?>" width="320" height="180" frameborder="0"></iframe>
			</div>
		<? buttons($writeable, $moderation, $mod); ?>
		</div>
	<? } ?>
	</div>
	<? } ?>
	</div>
<? } ?>
</div>

<?
function buttons($writeable, $moderation, $mod){
?>
	<? if(($writeable != false) || $moderation) { ?>
	<div class="buttons">
		<? if($writeable != false) { ?>
		<a class="ajax-action" href="<? echo Url::toRoute(['portfolio/remove', 'id'=>$mod->id]); ?>">Удалить</a>
		<? } ?>
		<? if($moderation) { ?>
		<a class="ajax-action" href="<? echo Url::toRoute(['portfolio/approve', 'id'=>$mod->id]); ?>">Утвердить</a>
		<? } ?>
	</div>
	<? } ?>
<?	
}