<?
use yii\helpers\Url;
use newerton\fancybox\FancyBox;

?>

<div class="inner-section clearfix">
<h3>Модерация портфолио</h3>
<?
echo FancyBox::widget([
    'target' => 'a[data-fancybox-group]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]);
?>
<div class="portfolio" data-ajax-embed="<? echo Url::toRoute(['portfolio/index', 'id'=>'unapproved', 'moderation'=>true])?>"></div>
</div>