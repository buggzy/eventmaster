<?php

/* @var $this \yii\web\View */
/* @var $content string */

namespace app\models;

use yii;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="cdQ5lRoRW5S0JUhQ7BeNZa93766gdPKpB_x6qf6aUv8" />
	<link rel="shortcut icon" href="/img/logo.png" />
 <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="vh-allwrap route-<?=preg_replace('/\//', '_', Yii::$app->controller->route) ?>">
	<? echo \Yii::$app->view->renderFile('@app/views/site/header.php'); ?>
	<?= $content ?>
	<? echo \Yii::$app->view->renderFile('@app/views/site/footer.php'); ?>
</div>
	        
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
