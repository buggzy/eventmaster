<?
use yii\grid\GridView;
use yii\i18n\Formatter;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
		<a href="<? echo Url::toRoute(['tags/view', 'id'=>'new'])?>">Добавить</a>

<?= GridView::widget([
    'dataProvider' => $model->_provider,
    'columns' => [
        'id',
        'title',
        'moderated:boolean',
        '_tags_buttons:raw'
    ],
    'formatter' => [
    	'class' => Formatter::class,
    	'booleanFormat'=>['Нет', 'Да']
    ]
]) ?>
</div>