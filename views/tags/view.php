<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<? echo $form->field($model, 'title')->textInput(); ?>
<? echo $form->field($model, 'moderated')->dropDownList(['0'=>'Нет', '1'=>'Да']); ?>

<?= Html::submitButton('Сохранить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>

<? ActiveForm::end(); ?>
</div>