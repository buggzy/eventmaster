<?
use yii\grid\GridView;
use app\models\User;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
	<a href="<? echo Url::toRoute(['eventtype/view', 'id'=>'new', 'parent'=>$parent])?>">Добавить</a>
	<? if($grand !== false) { ?>
	<a href="<? echo Url::toRoute(['eventtype/index', 'id'=>'new', 'parent'=>$grand])?>">Уровень вверх</a>
	<? } ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'title',
        'eventtypes_buttons:raw',
    ],
]) ?>
</div>