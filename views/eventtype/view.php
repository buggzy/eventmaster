<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\EventType;

$list = EventType::GetList($model->id);
$list[0] = 'Верхний уровень';


?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<? echo $form->field($model, 'title')->textInput(); ?>
<? echo $form->field($model, 'parent_id')->dropDownList($list); ?>

<?= Html::submitButton('Сохранить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>

<? ActiveForm::end(); ?>
</div>