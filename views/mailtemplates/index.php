<?
use yii\grid\GridView;
use yii\helpers\Url;

?>
<div class="inner-section clearfix">
	<a href="<? echo Url::toRoute(['mailtemplates/view', 'id'=>'new'])?>">Добавить</a>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'code',
        'title',
        '_templateslist_buttons:raw',
        // ...
    ],
]) ?>
</div>