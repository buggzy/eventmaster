<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\UserTypes;
use dosamigos\ckeditor\CKEditor;


?>
<div class="inner-section clearfix">
<? $form = ActiveForm::Begin(); ?>
<? echo $form->field($model, 'code')->textInput(); ?>
<? echo $form->field($model, 'title')->textInput(); ?>
<? echo $form->field($model, 'template_text')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic']) ?>
<? echo $form->field($model, 'comment')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic']) ?>
<?= Html::submitButton('Сохранить', ['name'=>'save', 'class' => 'btn btn-primary']) ?>

<? ActiveForm::end(); ?>
</div>