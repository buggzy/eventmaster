<?
namespace app\modules;

use Yii;

class MetaTags extends \yii\base\Module {
	
	public function __construct($id, $parent = null, $config = []){

		Yii::$app->view->registerMetaTag([
			'name'=>'description',
			'content'=>'Организация мероприятия в 1 клик! Воспользуйтесь сервисом Eventblaster и получайте лучшие предложения по организации Вашего мероприятия!',
			], 'meta_description');
		
		Yii::$app->view->registerMetaTag([
			'name'=>'og:image',
			'content'=>Yii::$app->request->hostInfo.'/img/eventblaster-image.jpg',
			], 'meta_og_image');


		return parent::__construct($id, $parent, $config);
			
	}
	
}