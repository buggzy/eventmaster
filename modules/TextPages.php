<?
namespace app\modules;

use Yii;
use app\models\InfoPages;

class TextPages extends \yii\base\Module {
	
	public function __construct($id, $parent = null, $config = []){
		
		$rules = [];
		
		foreach(InfoPages::GetList() as $item)
		{
			Yii::$app->getUrlManager()->addRules([
				$item->url => 'info/render'
    		], false);
		}
	
		

		return parent::__construct($id, $parent, $config);
	}
	
}