<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'debug', 'textpages', 'metatags'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'modules' => [
    	'textpages' => [
    			'class' => 'app\modules\TextPages'
    		],
    	'metatags' => [
    			'class' => 'app\modules\MetaTags'
    		]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'sRfBGczitWeSLSFemUIh1GHbNgvWtbD7',
        ],
        'recaptcha' => [
	        'class' => 'richweber\recaptcha\ReCaptcha',
	        'siteKey' => '6LeReiMTAAAAAPApj14tXUoi69mS_E1rW7qg2wvA',
	        'secretKey' => '6LeReiMTAAAAAF5tSZLPAi7Ho9XlngyhyYjpo3rf',
	        'errorMessage' => 'Подтвердите, что вы не робот',
	    ],
        'i18n' => [
	        'translations' => [
	            'app*' => [
	                'class' => 'yii\i18n\PhpMessageSource',
	                'basePath' => '@app/messages',
					'sourceLanguage' => 'en-US',
	                'fileMap' => [
	                    'app' => 'app.php',
	                    'app/error' => 'error.php',
	                ],
	            ],
	        ],
	    ],
        'sms' => [
	        'class'    => 'ladamalina\smsc\Smsc',
	        'login'     => 'rbandre',  // login
	        'password'   => 'prazdniki1512', // plain password or lowercase password MD5-hash
	        'post' => true, // use http POST method
	        'https' => true,    // use secure HTTPS connection
	        'charset' => 'utf-8',   // charset: windows-1251, koi8-r or utf-8 (default)
	        'debug' => false,    // debug mode
	    ],
	    'cache' => [
		'class' => 'yii\caching\FileCache',
	    ],
		'imageCache' => [
			'class' => 'iutbay\yii2imagecache\ImageCache',
			'sourcePath' => '@app/web/uploads',
			'sourceUrl' => '@web/uploads',
			'thumbsPath' => '@app/web/uploads/thumbs',
			'thumbsUrl' => '@web/uploads/thumbs',
			'sizes' => [
			    'thumb' => [150, 150],
			    'medium' => [300, 300],
			    'large' => [600, 600],
			],
		],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				'uploads/thumbs/<path:.*>' => 'site/thumb',
				'profile/<id:\d+>' => 'user/view',
				'orders/<id:\d+>' => 'orders/view',
				'order/<id:\d+>' => 'orders/index',
				'r-<id:\d+>' => 'site/ref',
				'refs/<id:\d+>' => 'refs/view',
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
		'allowedIPs' => ['94.31.163.83']
];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
