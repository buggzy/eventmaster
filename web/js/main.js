// ajax common and portfolio
$(function (){
	$('[data-ajax-embed]').each(function (i,e){
		(function (e){
			function reload(e){
				$(e).load($(e).data('ajax-embed'));
			}
			reload(e);
			$(e).on('reload', function (){
				reload(e);
			});
			$(e).on('click', 'a.ajax-action', function (ev){
				ev.preventDefault();
				$.get($(this).attr('href'), function (){
					reload(e);
				});
			});
			
		})(e);
	});	
	$('.pseudoform-submit').on('click', function (ev){
		ev.preventDefault();
		$form = $(this).closest('[data-pseudoform]');
		$url = $form.data('pseudoform');
		$.post($url, {addlink:$form.find('input[type=text]').val()}, function (){
			$form.find('input[type=text]').val('');
			$('.portfolio').trigger('reload');
		});
		
	});
	$('body').on('click', '.portfolio-toggle', function (ev){
		ev.preventDefault();
		$(this).toggleClass('active');
		var target = $($(this).attr('href'));
		target.load(target.data('url'));
	});

	$('.pass-unhide').click(function (ev){
		ev.preventDefault();
		$(this).addClass('hidden');
		var target = $($(this).attr('href'));
		target.removeClass('hidden');
	});

	$('body').on('click', '.expand-more-hint', function (ev){
		ev.preventDefault();
		$(this).addClass('clicked');
	});
	
	$('form .field-order-social label').click(function (){
		$(this).next().show();
	});

	
});


$(function (){
		$('body').on('click', '.ya-share2__link', function f()
		{
			$.getJSON('/payments/shared', function (data){
				if(data.redirect)
					location.href = data.redirect;
			});
		});
		
		$('.add_phone').click(function (ev){
			$(this).closest('.order-phones').addClass('new-phone');	
		});
		
		$('.get-sms-code').click(function (ev){
			ev.preventDefault();
			$.get('/site/phone',{phone:$('[name="Order[phone_add]"]').val()});
			$(this).html('Отправлен').prop('disabled', 'disabled');
			return false;
		});
		
		$('a.ajax-link').click(function (ev){
			ev.preventDefault();
			var elem = $(this);
			$.getJSON(elem.attr('href'), function (data){
				if(data.addClass)
					elem.addClass(data.addClass);
				if(data.removeClass)
					elem.removeClass(data.removeClass);
				if(data.setHtml)
					elem.html(data.setHtml);
			});
		});
		
		$('[type=radio][name="User[user_type_id]"]').hide().change(function (ev){
			$(this).closest('form').submit();	
		});
		
		if($('#vk_groups').length)
			VK.Widgets.Group("vk_groups", {mode: 2, wide: 1, width: '300', height: "400"}, 112828736);
	
		if($('#vk_comments').length){
			  VK.init({apiId: 5489945, onlyWidgets: true});
			  VK.Widgets.Comments("vk_comments", {limit: 10, width: "665", attach: "*"});
		}
	
});

// pinger
$(function (){

	var pingerInt = false;
	
	function interchange(){
		$.getJSON('/pinger', function (data){
			if(data.goAway)
				clearTimeout(pingerInt);

			var $messages_item = $('.main-menu li.messages a');
			if(0 === $messages_item.find('.count').length){
				$messages_item.append('<span class="count"></span>')
			}
			var $messages_count = $messages_item.find('.count');
				
			if(data.messages_unread > 0){
				$messages_count.html(' ('+data.messages_unread+' <i class="fa fa-envelope"></i>)');
			} else {
				$messages_count.html('');
			}
		});
	}
	
	interchange();
	pingerInt = setInterval(interchange, 55000);
	
});

// legacy govnocode

function showModal(a, b) {
	$('#overlay' + a).fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
		function() { // пoсле выпoлнения предъидущей aнимaции
			$(b + a)
				.css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
				.animate({
					opacity: 1,
					top: '40%'
				}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
		});
}

function fadeModal(a, b) {
	$(b + a)
		.animate({
				opacity: 0,
				top: '45%'
			}, 200, // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
			function() { // пoсле aнимaции
				$(this).css('display', 'none'); // делaем ему display: none;
				$('#overlay' + a).fadeOut(400); // скрывaем пoдлoжку
			}
		);
}

// end of legacy govnocode